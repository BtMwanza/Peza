import React, {useState, useCallback, useEffect} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {Dimensions} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import firebase from 'firebase/app';
import auth from '@react-native-firebase/auth';
import {useSelector, useDispatch} from 'react-redux';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import RNBootSplash from 'react-native-bootsplash';

import {DrawerContent} from './src/screens/DrawerContent';
import AppTabs from './src/stacks/AppTabs';
import AuthStack from './src/stacks/AuthStack';
import {Fire} from './src/components';
import {store, persistor} from './src/redux/Store';

import {Profile, Explore, Loading} from './src/screens';
import {fcmService} from './src/components/firebase/FCMService';
import {lnService} from './src/components/firebase/LNService';
import {Permission, PERMISSION_TYPE} from './src/utils/UserPermissions';
import {selectTheme} from './src/redux/reducers/ThemeSlice';

const {width, height} = Dimensions.get('screen');

function App({navigation}) {
  const [loaded, setLoaded] = useState(true);
  const [user, setUser] = useState(false);
  const [initializing, setInitializing] = useState(true);

  const Drawer = createDrawerNavigator();

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    RNBootSplash.hide({duration: 50});
    RNBootSplash.hide({fade: true});
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);

    GoogleSignin.configure({
      webClientId:
        '303734433656-bqn1j472g9uim0d8learqc557jtiojsp.apps.googleusercontent.com',
    });

    // TEST FOR NOTIFICATION
    // THIS CODE CAUSES THE APP TO CLOSE WHEN IMPLEMEMENTED, CAUSES UNKNOWN.
    /* try {
      fcmService.registerAppWithFCM();
      fcmService.register(onRegister, onNotification, onOpenNotification);
      lnService.configure(onOpenNotification);

      function onRegister(token) {
        console.log('[App] onRegister: ', token);
      }

      function onNotification(notify) {
        console.log('[App] onNotification: ', notify);
        const options = {
          soundName: 'default',
          playSound: true,
        };
        lnService.showNotification(
          0,
          notify.title,
          notify.body,
          notify,
          options,
        );
      }

      function onOpenNotification(notify) {
        console.log('[App] onOpenNotification: ', notify);
        alert('Open Notification: ' + notify.body);
      }
    } catch (error) {
      alert(error);
    } */

    return () => {
      console.log('[App] unRegister');
      //  fcmService.unRegister();
      //  lnService.unregister();
      //subscriber; // unsubscribe on unmount
    };
  }, []);

  if (initializing) return null;

  if (!loaded) {
    return <Loading />;
  }

  function AppStack() {
    //  const navigation = useNavigation();
    return (
      <Drawer.Navigator
        drawerStyle={{backgroundColor: 'transparent', width: '80%'}}
        overlayColor="transparent"
        drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen name="HomeDrawer" component={AppTabs} />
        <Drawer.Screen name="ExploreDrawer" component={Explore} />
        <Drawer.Screen name="ProfileDrawer" component={Profile} />
      </Drawer.Navigator>
    );
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          {user == null ? <AuthStack /> : <AppStack />}
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}

export default App;
