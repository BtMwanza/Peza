import React, {useEffect, useState} from 'react';
import {View, Text, Dimensions, StyleSheet} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import {Divider, Button} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Fontisto';

import {selectTheme} from '../../../redux/reducers/ThemeSlice';

const Complete = () => {
  const theme = useSelector(selectTheme);

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
          <Icon name="check" color={theme.onBackground} size={100} />
          <Button
            style={{borderColor: theme.text, marginVertical: 20}}
            color={theme.text}
            mode="outlined"
            onPress={() => {}}>
            Finished
          </Button>
        </View>
      </Container>
    </ThemeProvider>
  );
};

export default Complete;

const styles = StyleSheet.create({});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

const ButtonsBox = styled(Centered)`
  margin-top: 40px;
`;
