import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  TextInput,
  StyleSheet,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import {Button} from 'react-native-paper';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import shorthash from 'shorthash';
import firebase from 'firebase/app';
import moment from 'moment';

import {PLACEHOLDERS, BUTTONS} from '../../../shared';
import {selectTheme} from '../../../redux/reducers/ThemeSlice';
import {selectCart} from '../../../redux/reducers/CartSlice';
import {addToPayload} from '../../../redux';

const {width} = Dimensions.get('screen');

const Billing = () => {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const dispatch = useDispatch();
  const totalPrice = getTotal();

  const [data, setData] = useState({
    fullname: '',
    email: '',
    address: '',
    phoneNumber: '',
    check_nameChange: false,
    check_emailChange: false,
    check_addressChange: false,
    check_phoneChange: false,
  });

  const nameChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        fullname: val,
        check_nameChange: true,
      });
    } else {
      setData({
        ...data,
        fullname: val,
        check_nameChange: false,
      });
    }
  };

  const emailChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        email: val,
        check_emailChange: true,
      });
    } else {
      setData({
        ...data,
        email: val,
        check_emailChange: false,
      });
    }
  };

  const addressChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        address: val,
        check_addressChange: true,
      });
    } else {
      setData({
        ...data,
        address: val,
        check_addressChange: false,
      });
    }
  };

  const phoneChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        phoneNumber: val,
        check_phoneChange: true,
      });
    } else {
      setData({
        ...data,
        phoneNumber: val,
        check_phoneChange: false,
      });
    }
  };

  function getTotal() {
    return cart
      .reduce((acc, item) => {
        return acc + item.price;
      }, 0)
      .toFixed(2);
  }

  function commitTransaction(params) {
    try {
      const user = firebase.auth().currentUser;
      const cartID = 'tnx' + '_' + shorthash.random(12);
      firebase
        .firestore()
        .collection('TRANSACTIONS')
        .doc()
        .set({
          cartID: cartID,
          for: user.uid,
          cartRecord: cart,
          totalPrice: totalPrice,
          date: moment(Date.now()).format('lll'),
        });
    } catch (error) {
      alert(error);
    }
  }

  const payload = {
    tx_ref: 'tnx' + '_' + shorthash.random(12),
    amount: parseFloat(totalPrice),
    currency: 'ZMW',
    payment_options: 'mobilemoney',
    customer: {
      email: data.email,
      phoneNumber: data.phoneNumber,
      name: data.fullname,
    },
  };

  return (
    <ThemeProvider theme={theme}>
      <Container>
        {/* Billing form */}
        <ScrollView style={{paddingHorizontal: 5}}>
          {/* Full name */}
          <View style={styles.action}>
            <Icon name="person" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.fullname}
              placeholderTextColor="gray"
              style={[styles.textInput, {color: theme.text}]}
              autoCapitalize="none"
              onChangeText={val => nameChange(val)}
            />
            {data.check_nameChange ? (
              <Animatable.View animation="bounceIn">
                <Feather
                  name="check-circle"
                  color={theme.onBackground}
                  size={20}
                />
              </Animatable.View>
            ) : null}
          </View>

          {/* Email */}
          <View style={styles.action}>
            <Icon name="email" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.email}
              placeholderTextColor="gray"
              style={[styles.textInput, {color: theme.text}]}
              autoCapitalize="none"
              onChangeText={val => emailChange(val)}
            />
            {data.check_emailChange ? (
              <Animatable.View animation="bounceIn">
                <Feather
                  name="check-circle"
                  color={theme.onBackground}
                  size={20}
                />
              </Animatable.View>
            ) : null}
          </View>

          {/* Phone number */}
          <View style={styles.action}>
            <Icon name="phone" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.phone}
              placeholderTextColor="gray"
              style={[styles.textInput, {color: theme.text}]}
              autoCapitalize="none"
              onChangeText={val => phoneChange(val)}
            />
            {data.check_phoneChange ? (
              <Animatable.View animation="bounceIn">
                <Feather
                  name="check-circle"
                  color={theme.onBackground}
                  size={20}
                />
              </Animatable.View>
            ) : null}
          </View>

          {/* Street address */}
          <View style={styles.action}>
            <Icon name="map-marker-alt" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.address}
              placeholderTextColor="gray"
              style={[styles.textInput, {color: theme.text}]}
              autoCapitalize="none"
              onChangeText={val => addressChange(val)}
            />
            {data.check_addressChange ? (
              <Animatable.View animation="bounceIn">
                <Feather
                  name="check-circle"
                  color={theme.onBackground}
                  size={20}
                />
              </Animatable.View>
            ) : null}
          </View>

          <ButtonsBox>
            <Button
              style={{borderColor: theme.text, marginVertical: 20}}
              color={theme.text}
              mode="outlined"
              onPress={() => {
                dispatch(addToPayload(payload));
                commitTransaction();
              }}>
              {BUTTONS.proceed}
            </Button>
          </ButtonsBox>
        </ScrollView>
      </Container>
    </ThemeProvider>
  );
};

export default Billing;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: Platform.OS === 'ios' ? 3 : 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 0,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    fontSize: 18,
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
  },
  color_textPrivate: {
    color: 'grey',
  },
  errorMsg: {
    fontSize: 14,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

const ButtonsBox = styled(Centered)`
  margin-top: 10px;
`;
