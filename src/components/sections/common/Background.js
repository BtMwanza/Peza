import React, {memo} from 'react';
import {
  ImageBackground,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';

import {selectTheme} from '../../../redux/reducers/ThemeSlice';

import {FONTS, IMAGES, SIZES, ROUTES} from '../../../shared';

const Background = ({children}) => {
  const theme = useSelector(selectTheme);
  return (
    <ThemeProvider theme={theme}>
      <ImageBackground
        source={
          theme.state === false ? IMAGES.background : IMAGES.backgroundDark
        }
        resizeMode="repeat"
        style={styles.background}>
        <ScrollView>
          <KeyboardAvoidingView style={styles.container} behavior="padding">
            {children}
          </KeyboardAvoidingView>
        </ScrollView>
      </ImageBackground>
    </ThemeProvider>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    width: '100%',
  },
});

export default memo(Background);
