import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import {Svg, Polygon} from 'react-native-svg';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';

import {SIZES, FONTS, ROUTES} from '../../../shared';
import {selectTheme} from '../../../redux/reducers/ThemeSlice';
import {selectCart} from '../../../redux/reducers/CartSlice';
import CacheImage from '../../CacheImage';
import {
  selectProducts,
  fetchVendor,
} from '../../../redux/reducers/ProductSlice';
import {
  addItem,
  deleteItem,
  setVendorID,
  setCurrentProduct,
} from '../../../redux';

const {width} = Dimensions.get('screen');
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

function RecentProducts() {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const {recentProducts} = useSelector(selectProducts);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // Function to add product to the cart
  const addToCart = item => {
    try {
      dispatch(addItem(item));
      console.log('Current Cart: ', cart);
    } catch (error) {
      alert(error);
    }
  };

  // Function to remove product to the cart
  const removeFromCart = item => {
    let id = item.productID;
    try {
      dispatch(deleteItem(id));
    } catch (error) {
      alert(error);
    }
  };

  const toggleCart = item => {
    try {
      const added = cart.find(p => p.productID === item.productID);
      if (!added) {
        addToCart(item);
      } else {
        removeFromCart(item);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <Heading
          style={{
            ...FONTS.largeTitleBold,
          }}>
          RECENT
        </Heading>
        <ScrollContainer>
          {/* BODY */}

          {recentProducts.map(item => (
            <ProductBox
              style={{
                ...styles.shadow,
              }}>
              {/* SVG */}
              <SvgBox>
                <Svg height="100%" width="100%">
                  <Polygon
                    points="50,0 182,0 182,80"
                    fill="rgba(51,51,51, .5)"
                  />
                </Svg>
              </SvgBox>

              <LikeBox>
                <Icon
                  name={'heart'}
                  color={theme.text}
                  style={{backgroundColor: 'rgba(51,51,51, .5)'}}
                  size={20}
                />
              </LikeBox>
              <TouchableOpacity
                style={{marginHorizontal: 5}}
                onPress={() => {
                  dispatch(setVendorID(item.vendorID)),
                    dispatch(setCurrentProduct(item)),
                    dispatch(fetchVendor(item.vendorID)),
                    navigation.navigate(ROUTES.productDetails, {
                      item,
                    });
                }}>
                <CacheImage style={styles.image} uri={item.image} />
              </TouchableOpacity>

              <DetialsView>
                <ProductName
                  numberOfLines={2}
                  style={{
                    ...FONTS.h3,
                  }}>
                  {item.productName}
                </ProductName>

                <ProductPrice style={{...FONTS.body4}}>
                  K{item.price.toFixed(2)}
                </ProductPrice>
              </DetialsView>

              {/* ADD TO CART BUTTON */}
              <CartBox onPress={() => toggleCart(item)}>
                <Icon
                  name={'shopping-bag'}
                  color={theme.text}
                  style={{backgroundColor: 'transparent'}}
                  size={20}
                />
              </CartBox>
            </ProductBox>
          ))}
        </ScrollContainer>
      </Container>
    </ThemeProvider>
  );
}

export default RecentProducts;
const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3.84,
    elevation: 5,
  },
  image: {
    height: 80,
    aspectRatio: 1,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
`;

const ScrollContainer = styled.ScrollView.attrs(props => ({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
}))`
  flex: 1;
`;

const Heading = styled.Text`
  margin-top: ${SIZES.base}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ProductBox = styled.View`
  flex-direction: row;
  justify-content: space-between;
  background-color: ${props => props.theme.surface};
  width: ${width / 1.3}px;
  margin: 5px 5px 5px 10px;
  height: 120px;
  padding: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
`;

const ProductImage = styled.Image`
  height: 100px;
  aspect-ratio: 1;
  resize-mode: contain;
  align-self: center;
`;

const DetialsView = styled.View`
  flex: 1;
  margin-top: 15px;
  justify-content: space-between;
`;

const ProductName = styled.Text`
  color: ${props => props.theme.onSurface};
  flex-wrap: wrap;
`;

const ProductDesc = styled.Text`
  font-size: ${EStyleSheet.value('1rem')}px;
  opacity: 0.7;
  z-index: 1;
  color: ${props => props.theme.onSurface};
`;

const ProductPrice = styled.Text`
  font-size: 16px;
  color: ${props => props.theme.onSurface};
`;

const CartBox = styled.TouchableOpacity`
  aspect-ratio: 1;
  background-color: ${props => props.theme.surface};
  position: absolute;
  bottom: 10px;
  right: 10px;
  justify-content: center;
  align-items: center;
`;

const LikeBox = styled.TouchableOpacity`
  aspect-ratio: 1;
  background-color: ${props => props.theme.surface};
  position: absolute;
  top: 5px;
  right: 10px;
  justify-content: center;
  align-items: center;
`;

const SvgBox = styled.View`
  position: absolute;
  top: 0px;
  right: 0px;
  width: ${WIDTH / 2.35}px;
  height: 240px;
`;
