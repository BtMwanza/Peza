import React from 'react';
import {Polygon, Svg} from 'react-native-svg';
import CacheImage from './../../CacheImage';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import styled, {ThemeProvider} from 'styled-components';
import Swiper from 'react-native-swiper';
import {
  Dimensions,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
  View,
} from 'react-native';

import {FONTS, IMAGES, SIZES, ROUTES} from '../../../shared';
import {selectTheme} from '../../../redux/reducers/ThemeSlice';
import {selectCart} from '../../../redux/reducers/CartSlice';
import {
  selectProducts,
  fetchVendor,
} from '../../../redux/reducers/ProductSlice';
import {
  addItem,
  deleteItem,
  setVendorID,
  setCurrentProduct,
} from '../../../redux';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const Popular = () => {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const {recentProducts} = useSelector(selectProducts);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  return (
    <ThemeProvider theme={theme}>
      <View>
        <Heading
          style={{
            ...FONTS.largeTitleBold,
          }}>
          POPULAR
        </Heading>
        <Container>
          <PopularBox>
            <Swiper
              showsButtons={false}
              showsPagination={false}
              autoplay={true}
              loop={true}
              horizontal={true}
              style={{height: HEIGHT / 7}}>
              {recentProducts.map(item => (
                <TouchableOpacity
                  style={{...styles.shadow}}
                  onPress={() => {
                    dispatch(setVendorID(item.vendorID)),
                      dispatch(setCurrentProduct(item)),
                      dispatch(fetchVendor(item.vendorID)),
                      navigation.navigate(ROUTES.productDetails, {
                        item,
                      });
                  }}>
                  <CacheImage
                    uri={item.image}
                    resizeMode="contain"
                    style={styles.image}
                  />

                  {/* <PopularCircle
                    source={{uri: item.image}}
                    resizeMode="contain"
                  /> */}
                  <DetailsBox>
                    <Text
                      numberOfLines={2}
                      style={[
                        styles.itemText,
                        {color: theme.text, ...FONTS.h3},
                      ]}>
                      {item.productName}
                    </Text>
                    <Text
                      style={[
                        styles.itemText,
                        {color: theme.text, ...FONTS.h5},
                      ]}>
                      K{item.price}
                    </Text>
                  </DetailsBox>
                </TouchableOpacity>
              ))}
            </Swiper>
          </PopularBox>
        </Container>
      </View>
    </ThemeProvider>
  );
};

export default Popular;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3.84,
    elevation: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginHorizontal: 3,
    height: 85,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemText: {
    marginVertical: 5,
  },
  image: {
    height: 50,
    width: 50,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const ScrollContainer = styled.ScrollView.attrs(props => ({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
}))`
  flex: 1;
  padding-horizontal: ${SIZES.radius}px;
`;

const Heading = styled.Text`
  margin-top: ${SIZES.base}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const PopularBox = styled.TouchableOpacity`
  flex: 1;
  background-color: ${props => props.theme.surface};
  margin-horizontal: 3.4px;
  margin-vertical: 5px;
  padding-horizontal: 10px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  height: 85px;
  border-radius: 50px;
  border-color: ${props => props.theme.secondary};
  border-width: 0.7px;
`;

const PopularCircle = styled.Image`
  height: 50px;
  width: 50px;
`;

const DetailsBox = styled.View`
  flex: 1;
  margin-vertical: 10px;
  margin-horizontal: 10px;
  justify-content: space-between;
`;
