import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Polygon, Svg} from 'react-native-svg';
import Icon from 'react-native-vector-icons/Feather';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import styled, {ThemeProvider} from 'styled-components';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {FONTS, IMAGES, SIZES, ROUTES} from '../../../shared';
import {selectTheme} from '../../../redux/reducers/ThemeSlice';
import {selectCart} from '../../../redux/reducers/CartSlice';
import {
  selectProducts,
  fetchVendor,
} from '../../../redux/reducers/ProductSlice';
import {
  addItem,
  deleteItem,
  setVendorID,
  setCurrentProduct,
} from '../../../redux';

const {width, height} = Dimensions.get('screen');
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const LimitedDealsSection = () => {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const {products} = useSelector(selectProducts);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // Dummy Data
  const [promo, setPromo] = React.useState([
    {
      productID: 23,
      productName: 'BMW Engine',
      oldPrice: 9000,
      price: 5000,
      image: IMAGES.bmwEngine,
      currentQuantity: 1,
      category: 'Engine & Emissions',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 24,
      productName: 'Bosch Double Iriduim Spark Plug',
      oldPrice: 200,
      price: 150,
      image: IMAGES.boschDoubleIriduimSparkPlug,
      currentQuantity: 1,
      category: 'Engine & Emissions',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 25,
      productName: 'Bosch Spark Plug',
      oldPrice: 199.99,
      price: 89.99,
      image: IMAGES.boschSparkPlug,
      currentQuantity: 1,
      category: 'Engine & Emissions',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 26,
      productName: 'Headlight',
      oldPrice: 320,
      price: 220,
      image: IMAGES.carHeadlights,
      currentQuantity: 1,
      category: 'Lamps & Lighting',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 27,
      productName: 'Dellorto Caburator',
      oldPrice: 550,
      price: 390,
      image: IMAGES.carburator,
      currentQuantity: 1,
      category: 'Engine & Emissions',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 28,
      productName: '2003-2008 Toyota Corolla Headlights',
      oldPrice: 300,
      price: 210,
      image: IMAGES.toyotaCorollaHeadlight03_08,
      currentQuantity: 1,
      category: 'Lamps & Lighting',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
  ]);

  // Function to add product to the cart
  const addToCart = item => {
    try {
      dispatch(addItem(item));
      console.log('Current Cart: ', cart);
    } catch (error) {
      alert(error);
    }
  };

  // Function to remove product to the cart
  const removeFromCart = item => {
    let id = item.productID;
    try {
      dispatch(deleteItem(id));
    } catch (error) {
      alert(error);
    }
  };

  const toggleCart = item => {
    try {
      const added = cart.find(p => p.productID === item.productID);
      if (!added) {
        addToCart(item);
      } else {
        removeFromCart(item);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // MAIN VIEW
  return (
    <ThemeProvider theme={theme}>
      <View>
        <Heading
          style={{
            ...FONTS.largeTitleBold,
          }}>
          LIMITED DEALS
        </Heading>
        <ScrollContainer>
          {/* BODY */}

          {promo.map(item => (
            <ProductBox
              style={{
                ...styles.shadow,
              }}>
              {/* SVG */}
              <SvgBox>
                <Svg height="100%" width="100%">
                  <Polygon
                    points="0,0 182,0 182,120"
                    fill="rgba(51,51,51, .5)"
                  />
                </Svg>
              </SvgBox>
              <TouchableOpacity onPress={() => {}}>
                <ProductImage
                  source={item.image}
                  style={{
                    height: 100,
                    aspectRatio: 1,
                    resizeMode: 'contain',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>

              {/* OLD PRICE */}
              <OldPriceBox>
                <OldPrice
                  style={{
                    ...FONTS.body4,
                  }}>
                  K{item.oldPrice.toFixed(2)}
                </OldPrice>
              </OldPriceBox>

              <DetialsView>
                <ProductName
                  numberOfLines={2}
                  style={{
                    ...FONTS.h3,
                  }}>
                  {item.productName}
                </ProductName>

                {/* <ProductDesc
                  numberOfLines={2}
                  style={{
                    ...FONTS.body4,
                  }}>
                  {item.desc}
                </ProductDesc> */}
                {/* NEW PRICE */}
                <ProductPrice
                  style={{
                    ...FONTS.body4,
                  }}>
                  K{item.price.toFixed(2)}
                </ProductPrice>
              </DetialsView>

              {/* ADD TO CART BUTTON */}
              <CartBox onPress={() => toggleCart(item)}>
                <Icon name={'shopping-bag'} color={theme.text} size={20} />
              </CartBox>
            </ProductBox>
          ))}
        </ScrollContainer>
      </View>
    </ThemeProvider>
  );
};

export default LimitedDealsSection;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
`;

const ScrollContainer = styled.ScrollView.attrs(props => ({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
}))`
  flex: 1;
`;

const Heading = styled.Text`
  margin-top: ${SIZES.base}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ProductBox = styled.View`
  background-color: ${props => props.theme.surface};
  width: ${SIZES.cardWidth}px;
  margin: 5px 5px 5px 10px;
  height: ${SIZES.cardHeight}px;
  padding: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
`;

const ProductImage = styled.Image`
  height: 100px;
  aspect-ratio: 1;
  resize-mode: contain;
  align-self: center;
`;

const DetialsView = styled.View`
  flex: 1;
  margin-top: 10px;
  justify-content: space-between;
`;

const ProductName = styled.Text`
  color: ${props => props.theme.onSurface};
  flex-wrap: wrap;
`;

const ProductDesc = styled.Text`
  font-size: ${EStyleSheet.value('1rem')}px;
  opacity: 0.7;
  z-index: 1;
  color: ${props => props.theme.onSurface};
`;

const ProductPrice = styled.Text`
  font-size: 16px;

  color: ${props => props.theme.onSurface};
`;

const OldPriceBox = styled.View`
  position: absolute;
  top: 0px;
  margin-left: 8px;
`;

const OldPrice = styled.Text`
  font-size: 16px;
  text-decoration-line: line-through;
  text-decoration-style: solid;
  color: ${props => props.theme.onSurface};
`;

const CartBox = styled.TouchableOpacity`
  aspect-ratio: 1;
  background-color: ${props => props.theme.surface};
  position: absolute;
  bottom: 10px;
  right: 10px;
  justify-content: center;
  align-items: center;
`;

const SvgBox = styled.View`
  position: absolute;
  top: 0px;
  right: 0px;
  width: ${WIDTH / 2.35}px;
  height: 240px;
`;
