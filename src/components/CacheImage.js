import React, {Component} from 'react';
import {Image} from 'react-native';
import shorthash from 'shorthash';
import {Dirs, FileSystem} from 'react-native-file-access';

export default class CacheImage extends Component {
  state = {
    source: null,
  };

  caching = async () => {
    const {uri} = this.props;
    const name = shorthash.unique(uri);
    const path = `${Dirs.CacheDir}/${name}+'.png'`;

    const doesImageExist = await FileSystem.exists(path);
    if (doesImageExist === true) {
      const image = await FileSystem.readFile(path);
      this.setState({
        source: {
          uri: image,
        },
      });
      return;
    } else {
      /* Download image if one doesn't exist */
      const newImage = await FileSystem.fetch(uri, {path: path})
        .then(response => {
          return response;
        })
        .then(data => {
          return data;
        });
      FileSystem.writeFile(path, newImage.url);

      this.setState({
        source: {
          uri: newImage.url,
        },
      });
    }
  };

  componentDidMount = () => {
    this.caching();
  };

  render() {
    return <Image style={this.props.style} source={this.state.source} />;
  }
}
