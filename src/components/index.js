// SECTIONS
export {default as LimitedDealsSection} from './sections/common/LimitedDealsSection';
export {default as RecentProducts} from './sections/common/RecentProducts';
export {default as Popular} from './sections/common/Popular';

export {default as Billing} from './sections/pay/Billing';
export {default as Complete} from './sections/pay/Complete';

// ICONS
export {default as CartIcon} from './header/CartIcon';
export {default as SearchIcon} from './header/SearchIcon';
export {default as Spinner} from './Spinner';

// BUTTONS
export {default as GoogleAuthButton} from './buttons/GoogleAuthButton';

// THEMES
export {lightTheme, darkTheme} from './Themes';

// AUTHENTICATION
export {AuthContext} from './Context';

// FIREBASE CONFIG
export {default as Fire} from './firebase/firebaseConfig';
