/* eslint-disable react/prop-types */
import React from 'react';
import {StyleSheet} from 'react-native';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-google-signin/google-signin';

import {Fire} from '../../components';

export default function GoogleAuthButton(props) {
  return (
    <GoogleSigninButton
      style={styles.button}
      size={GoogleSigninButton.Size.Wide}
      color={GoogleSigninButton.Color.Dark}
      onPress={() => {
        Fire.shared.createWithGoogle();
      }}
    />
  );
}

const styles = StyleSheet.create({
  button: {
    width: 198,
    height: 50,
    marginBottom: 20,
  },
});
