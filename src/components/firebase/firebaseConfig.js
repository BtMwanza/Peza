import firebase from 'firebase/app';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
require('firebase/firestore');

class Fire {
  constructor() {
    this.init();
  }

  init = () => {
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: env.process.API_KEY,
        authDomain: env.process.AUTH_DOMAIN,
        databaseURL: env.process.DATABASE_URL,
        projectId: env.process.PROJECT_ID,
        storageBucket: env.process.STORAGE_BUCKET,
        messagingSenderId: env.process.MESSAGING_SENDER_ID,
        appId: env.process.APP_ID,
        measurementId: env.process.MEASUREMENT_ID,
      });
      firebase.firestore().settings({experimentalForceLongPolling: true});
    }
  };

  subscribeToAuthChanges(authStateChanged) {
    auth().onAuthStateChanged(user => {
      authStateChanged(user);
    });
  }

  uploadPhotoAsync = (uri, filename) => {
    return new Promise(async (res, rej) => {
      const response = await fetch(uri);
      const file = await response.blob();

      let upload = firebase.storage().ref(filename).put(file);

      upload.on(
        'state_changed',
        snapshot => {},
        err => {
          rej(err);
        },
        async () => {
          const url = await upload.snapshot.ref.getDownloadURL();
          res(url);
        },
      );
    });
  };

  // TEST FOR ACCOUNT CREATION
  createUser = async (email, username, password, phoneNumber) => {
    try {
      await auth()
        .createUserWithEmailAndPassword(email.trim(), password)
        .then(userInfo => {
          // Signed in
          userInfo.user.updateProfile({
            displayName: username,
            photoURL: null,
          });
        });

      let user = auth().currentUser;

      let db = firebase.firestore().collection('USERS').doc(user.uid);

      db.set({
        displayName: username,
        email: email,
        phoneNumber: phoneNumber,
        avatar: null,
        userID: user.uid,
      });

      /* 
       // Send verification email
      var user = firebase.auth().currentUser;

      var actionCodeSettings = {
        url:
          'https://peza-e3455.firebaseapp.com/__/auth/action?mode=action&oobCode=code',
        iOS: {
          bundleId: 'com.peza',
        },
        android: {
          packageName: 'com.peza',
          installApp: true,
        },
        handleCodeInApp: true,
      };

      user
        .sendEmailVerification(actionCodeSettings)
        .then(function () {
          // Email sent.
        })
        .catch(function (error) {
          alert(error.code, ': ', error.message);
        }); */
    } catch (error) {
      alert('Register error: ', error);
    }
  };

  // Google sign in
  createWithGoogle = async () => {
    try {
      // Get the users ID token
      const {idToken} = await GoogleSignin.signIn();

      // Create a Google credential with the token
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      // Sign-in the user with the credential
      auth()
        .signInWithCredential(googleCredential)
        .then(() => {
          let user = auth().currentUser;
          console.log('AUTH_USER: ', user.displayName);

          let db = firebase.firestore().collection('USERS').doc(user.uid);

          db.set({
            displayName: user.displayName,
            email: user.email,
            phoneNumber: user.phoneNumber,
            avatar: user.photoURL,
            userID: user.uid,
          });
        });
    } catch (error) {
      alert(error);
    }
  };

  // Database reference
  /*  ref() {
    return firebase.database().ref();
  } */

  // Turn off listening
  off() {
    this.ref.off();
  }

  signOut = async () => {
    auth().signOut();
    if (GoogleSignin) {
      try {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      } catch (error) {
        alert(error);
      }
    }
  };

  // TEST FOR REVOKING GOOGLE ACCESS AFTER LOGGING OUT
  revokeAccess = async () => {
    try {
      await GoogleSignin.revokeAccess();
      console.log('deleted');
    } catch (error) {
      console.error(error);
    }
  };

  get firestore() {
    return firebase.firestore();
  }

  get uid() {
    return (auth().currentUser || {}).uid;
  }

  get timestamp() {
    return Date.now();
  }
}

Fire.shared = new Fire();
export default Fire;
