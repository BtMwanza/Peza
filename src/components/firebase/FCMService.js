import FCMServicebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
import {Platform} from 'react-native';

class FCMService {
  // CLOUD MESSAGING
  register = (onRegister, onNotification, onOpenNotification) => {
    this.checkPermission(onRegister);
    this.createNotificationListeners(
      onRegister,
      onNotification,
      onOpenNotification,
    );
  };

  registerAppWithFCM = async () => {
    if (Platform.OS === 'ios') {
      await messaging().registerDeviceForRemoteMessages();
      await messaging().setAutoInitEnabled(true);
    }
  };

  checkPermission = onRegister => {
    messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          // User has permissions
          this.getToken(onRegister);
        } else {
          // User doesn't have permission
          this.requestPermission(onRegister);
        }
      })
      .catch(error => {
        alert('[FCMService] Permission rejected ', error.message);
      });
  };

  getToken = onRegister => {
    messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
          alert('[FCMService] User does not have a device token');
        }
      })
      .catch(error => {
        alert('[FCMService] getToken rejected ', error.massage);
      });
  };

  requestPermission = onRegister => {
    messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch(error => {
        alert('[FCMService] Request Permission rejected ', error.massage);
      });
  };

  deleteToken = () => {
    console.log('[FCMService] deleteToken ');
    messaging()
      .deleteToken()
      .catch(error => {
        alert('[FCMService] Delete token error ', error.massage);
      });
  };

  createNotificationListeners = (
    onRegister,
    onNotification,
    onOpenNotification,
  ) => {
    // When the app is running, but in the background
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        '[FCMService] onNotificationOpenedApp Notification caused app to open',
      );
      if (remoteMassage) {
        const notification = remoteMessage.notification;
        onOpenNotification(notification);
      }
    });

    // When the app is opened from a quite state
    messaging()
      .getInitialNotification()
      .then(remoteMassage => {
        console.log(
          '[FCMService] getInitialNotification Notofication caused app to open',
        );

        if (remoteMassage) {
          const notification = remoteMessage.notification;
          onOpenNotification(notification);
        }
      });

    // Foreground state messages
    this.messageListner = messaging().onMessage(async remoteMassage => {
      console.log('[FCMService] A new FCM message arrived ', remoteMassage);
      if (remoteMessage) {
        let notification = null;
        if (Platform.OS === 'ios') {
          notification = remoteMassage.data.notification;
        } else {
          notification = remoteMassage.notification;
        }
      }
    });

    // Triggered when there is a new token
    messaging().onTokenRefresh(fcmToken => {
      console.log('[FCMService] New token refresh: ', fcmToken);
      onRegister(fcmToken);
    });
  };

  unRegister = () => {
    this.messageListner();
  };
}

export const fcmService = new FCMService();
