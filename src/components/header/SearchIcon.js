import React from 'react';
import Icon from 'react-native-vector-icons/Fontisto';
import {useSelector, useDispatch} from 'react-redux';
import {ROUTES} from './../../shared';

function SearchIcon(props) {
  const theme = useSelector(state => state.themer.theme);
  return (
    <Icon.Button
      name="search"
      size={22}
      color={theme.text}
      backgroundColor={theme.top_tab}
      onPress={props.onPress}
      {...props}
    />
  );
}

export default SearchIcon;
