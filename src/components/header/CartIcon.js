import React from 'react';
import {
  View,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import {ROUTES, COLORS, SIZES, FONTS} from './../../shared';

function CartIcon(props) {
  const theme = useSelector(state => state.themer.theme);
  const cart = useSelector(state => state.cart.cart);
  return (
    <ThemeProvider theme={theme}>
      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          props.navigation.navigate(ROUTES.cart);
        }}>
        <Indicator>
          <Text style={{color: theme.onSecondary, fontSize: SIZES.body4}}>
            {cart.length}
          </Text>
        </Indicator>
        <Icon.Button
          name="shopping-cart"
          size={25}
          color={theme.text}
          backgroundColor={theme.top_tab}
        />
      </TouchableOpacity>
    </ThemeProvider>
  );
}

export default CartIcon;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const Indicator = styled.View`
  position: absolute;
  height: 30px;
  width: 30px;
  border-radius: 15px;
  background-color: ${props => props.theme.secondary};
  opacity: 0.9;
  left: 15px;
  bottom: 17px;
  align-items: center;
  justify-content: center;
  z-index: 1;
  color: ${props => props.theme.onSecondary};
`;

// rgba(19, 197, 123, 0.8)
