import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {useSelector, useDispatch} from 'react-redux';
import {ROUTES} from './../../shared';

function MenuIcon({props, onPress}) {
  const theme = useSelector(state => state.themer.theme);
  return (
    <Icon.Button
      name="menu"
      size={25}
      color={theme.text}
      backgroundColor={theme.top_tab}
      onPress={onPress}
      {...props}
    />
  );
}

export default MenuIcon;
