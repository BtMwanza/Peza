import React from 'react';
import {View, StyleSheet} from 'react-native';
import {ActivityIndicator, Colors, Text} from 'react-native-paper';

const Spinner = () => (
  <View style={styles.container}>
    <ActivityIndicator animating={true} size="large" />
    <Text>Loading</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Spinner;
