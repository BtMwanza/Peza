/* eslint-disable react/prop-types */
import React from 'react';
import {StyleSheet, View, Text, StatusBar, Dimensions} from 'react-native';
import {useSelector} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import {Button} from 'react-native-paper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';

import {GoogleAuthButton} from './../../components';
import {selectTheme} from './../../redux/reducers/ThemeSlice';
import {MESSAGES, PLACEHOLDERS, ROUTES, BUTTONS} from '../../shared';

const {width} = Dimensions.get('screen');

const SignIn = ({navigation: {navigate}}) => {
  const [data, setData] = React.useState({
    email: '',
    password: '',
    check_emailChange: false,
    secureTextEntry: true,
    isValidEmail: true,
    isValidPassword: true,
  });

  const theme = useSelector(selectTheme);

  // Handle text input changes
  const emailChange = val => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        email: val,
        check_emailChange: true,
        isValidEmail: true,
      });
    } else {
      setData({
        ...data,
        email: val,
        check_emailChange: false,
        isValidEmail: false,
      });
    }
  };

  const handlePasswordChange = val => {
    if (val.trim().length >= 8) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };

  // Show or hide password
  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  // Validate email
  const handleValidUser = val => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        isValidEmail: true,
      });
    } else {
      setData({
        ...data,
        isValidEmail: false,
      });
    }
  };

  // Sign in user using email and password
  const handleSubmit = async (email, password) => {
    try {
      await auth().signInWithEmailAndPassword(email.trim(), password);
    } catch (error) {
      alert(error);
    }
  };

  // LOGIN FORM
  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />

        <Animatable.View animation="fadeIn" style={styles.form}>
          {/* EMAIL */}
          <View style={styles.action}>
            <Icon name="person" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.email}
              placeholderTextColor={theme.accent}
              keyboardType="email-address"
              autoCapitalize="none"
              onChangeText={val => emailChange(val)}
              onEndEditing={e => handleValidUser(e.nativeEvent.text)}
            />
            {data.check_emailChange ? (
              <Animatable.View animation="bounceIn">
                <Feather
                  name="check-circle"
                  color={theme.onBackground}
                  size={20}
                />
              </Animatable.View>
            ) : null}
          </View>

          {/* Show validation massage */}
          {data.isValidEmail ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={[styles.errorMsg, {color: theme.error}]}>
                email must be 4 characters long.
              </Text>
            </Animatable.View>
          )}

          {/* PASSWORD */}
          <View style={styles.action}>
            <Icon name="locked" color={theme.text} size={20} />
            <TextInput
              placeholderTextColor={theme.accent}
              placeholder={PLACEHOLDERS.password}
              autoCapitalize="none"
              onChangeText={val => handlePasswordChange(val)}
              secureTextEntry={data.secureTextEntry ? true : false}
            />

            <TouchableOpacity onPress={updateSecureTextEntry}>
              {data.secureTextEntry ? (
                <Feather name="eye-off" color={theme.onBackground} size={20} />
              ) : (
                <Feather name="eye" color={theme.onBackground} size={20} />
              )}
            </TouchableOpacity>
          </View>

          {/* Show validation massage */}
          {data.isValidPassword ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={[styles.errorMsg, {color: theme.error}]}>
                Password must be 8 characters long.
              </Text>
            </Animatable.View>
          )}

          {/* Buttons */}
          <ButtonsBox>
            <Button
              mode="contained"
              onPress={() => {
                handleSubmit(data.email, data.password);
              }}>
              {BUTTONS.login}
            </Button>

            <Button
              style={{borderColor: theme.text, marginVertical: 15}}
              color={theme.text}
              mode="outlined"
              onPress={() => navigate(ROUTES.register)}>
              {MESSAGES.register}
            </Button>
          </ButtonsBox>

          <ButtonsBox>
            <GoogleAuthButton />
          </ButtonsBox>
        </Animatable.View>
      </Container>
    </ThemeProvider>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  form: {
    flex: Platform.OS === 'ios' ? 3 : 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 0,
    marginTop: 20,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text_form: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
  },
  color_textPrivate: {
    color: 'grey',
  },
  errorMsg: {
    fontSize: 14,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

export const TextInput = styled.TextInput.attrs(props => ({
  autoCorrect: false,
  autoCapitalize: 'none',
  returnKeyType: 'done',
  placeholder: props.placeholder,
  secureTextEntry: props.secureTextEntry,
  keyboardType: props.keyboardType,
}))`
  flex: 1;
  margin-top: ${Platform.OS === 'ios' ? 0 : -12}px;
  padding-left: 10px;
  font-size: 18px;
  color: ${props => props.theme.text};
`;

const ButtonsBox = styled(Centered)`
  margin-top: 20px;
`;
