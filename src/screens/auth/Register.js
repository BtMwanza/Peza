/* eslint-disable react/prop-types */
import React from 'react';
import {StyleSheet, View, Text, StatusBar, Dimensions} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import Animated, {Easing} from 'react-native-reanimated';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import {Button} from 'react-native-paper';
import {
  TapGestureHandler,
  State,
  TouchableOpacity,
} from 'react-native-gesture-handler';

import {AuthContext, Fire, GoogleAuthButton} from './../../components';
import {MESSAGES, COLORS, PLACEHOLDERS, ROUTES, BUTTONS} from '../../shared';
import {selectTheme} from './../../redux/reducers/ThemeSlice';

const {width} = Dimensions.get('screen');

const SignUp = ({navigation: {navigate}}) => {
  const [data, setData] = React.useState({
    email: '',
    username: '',
    password: '',
    confirmPassword: '',
    phoneNumber: '',
    checkTextInputChange: false,
    checkEmailChange: false,
    checkPhoneInputChange: false,
    secureTextEntry: true,
    confirmSecureTextEntry: true,
    isValidEmail: true,
    isValidPassword: true,
  });

  const theme = useSelector(selectTheme);

  const textInputChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        username: val,
        checkTextInputChange: true,
      });
    } else {
      setData({
        ...data,
        username: val,
        checkTextInputChange: false,
      });
    }
  };

  const phoneInputChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        phoneNumber: val,
        checkPhoneInputChange: true,
      });
    } else {
      setData({
        ...data,
        phoneNumber: val,
        checkPhoneInputChange: false,
      });
    }
  };

  const emailChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        email: val,
        checkEmailChange: true,
      });
    } else {
      setData({
        ...data,
        email: val,
        checkEmailChange: false,
      });
    }
  };

  const handlePasswordChange = val => {
    if (val.trim().length >= 8) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };

  const handleConfirmPasswordChange = val => {
    if (val.trim().length >= 8) {
      setData({
        ...data,
        confirmPassword: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        confirmPassword: val,
        isValidPassword: false,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const updateConfirmSecureTextEntry = () => {
    setData({
      ...data,
      confirmSecureTextEntry: !data.confirmSecureTextEntry,
    });
  };

  const handleValidEmail = val => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        isValidEmail: true,
      });
    } else {
      setData({
        ...data,
        isValidEmail: false,
      });
    }
  };

  const handleSubmit = (email, username, password, phoneNumber) => {
    if (password === data.confirmPassword) {
      Fire.shared.createUser(email, username, password, phoneNumber);
    } else {
      alert('Error: Password does not match');
    }
  };

  // REGISTRATION FORM
  return (
    <ThemeProvider theme={theme}>
      <ScrollContainer>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />
        <Animatable.View animation="zoomIn" style={styles.form}>
          {/* Username */}
          <View style={styles.action}>
            <Icon name="person" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.username}
              placeholderTextColor={theme.accent}
              autoCapitalize="none"
              onChangeText={val => textInputChange(val)}
            />
            {data.checkTextInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather
                  name="check-circle"
                  color={theme.onBackground}
                  size={20}
                />
              </Animatable.View>
            ) : null}
          </View>

          {/* Email */}
          <View style={styles.action}>
            <Icon name="email" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.email}
              placeholderTextColor={theme.accent}
              keyboardType="email-address"
              autoCapitalize="none"
              onChangeText={val => emailChange(val)}
              onEndEditing={e => handleValidEmail(e.nativeEvent.text)}
            />
            {data.checkEmailChange ? (
              <Animatable.View animation="bounceIn">
                <Feather
                  name="check-circle"
                  color={theme.onBackground}
                  size={20}
                />
              </Animatable.View>
            ) : null}
          </View>

          {/* Show validation massage */}
          {data.isValidEmail ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={[styles.errorMsg, {color: theme.error}]}>
                email must be 4 characters long.
              </Text>
            </Animatable.View>
          )}

          {/* Phone Number */}
          <View style={styles.action}>
            <Icon name="phone" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.phone}
              placeholderTextColor={theme.accent}
              keyboardType="phone-pad"
              autoCapitalize="none"
              onChangeText={val => phoneInputChange(val)}
            />
            {data.checkPhoneInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather
                  name="check-circle"
                  color={theme.onBackground}
                  size={20}
                />
              </Animatable.View>
            ) : null}
          </View>

          {/* Password */}
          <View style={styles.action}>
            <Icon name="locked" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.password}
              placeholderTextColor={theme.accent}
              secureTextEntry={data.secureTextEntry ? true : false}
              autoCapitalize="none"
              onChangeText={val => handlePasswordChange(val)}
            />
            <TouchableOpacity onPress={updateSecureTextEntry}>
              {data.secureTextEntry ? (
                <Feather name="eye-off" color={theme.onBackground} size={20} />
              ) : (
                <Feather name="eye" color={theme.onBackground} size={20} />
              )}
            </TouchableOpacity>
          </View>

          {/* Confirm Number */}
          <View style={styles.action}>
            <Icon name="locked" color={theme.text} size={20} />
            <TextInput
              placeholder={PLACEHOLDERS.confirmPassword}
              placeholderTextColor={theme.accent}
              secureTextEntry={data.confirmSecureTextEntry ? true : false}
              autoCapitalize="none"
              onChangeText={val => handleConfirmPasswordChange(val)}
            />
            <TouchableOpacity onPress={updateConfirmSecureTextEntry}>
              {data.secureTextEntry ? (
                <Feather name="eye-off" color={theme.onBackground} size={20} />
              ) : (
                <Feather name="eye" color={theme.onBackground} size={20} />
              )}
            </TouchableOpacity>
          </View>

          {/* Buttons */}
          <ButtonsBox>
            <Button
              style={{marginVertical: 15}}
              mode="contained"
              onPress={() => {
                handleSubmit(
                  data.email,
                  data.username,
                  data.password,
                  data.phoneNumber,
                );
              }}>
              Sign Up
            </Button>

            <Button
              style={{borderColor: theme.text}}
              color={theme.text}
              mode="outlined"
              onPress={() => navigate(ROUTES.login)}>
              {MESSAGES.already}
            </Button>
          </ButtonsBox>
        </Animatable.View>
      </ScrollContainer>
    </ThemeProvider>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  form: {
    flex: Platform.OS === 'ios' ? 3 : 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 0,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text_form: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
  },
  color_textPrivate: {
    color: 'grey',
  },
  errorMsg: {
    fontSize: 14,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ScrollContainer = styled.ScrollView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

export const TextInput = styled.TextInput.attrs(props => ({
  autoCorrect: false,
  autoCapitalize: 'none',
  returnKeyType: 'done',
  placeholder: props.placeholder,
  secureTextEntry: props.secureTextEntry,
  keyboardType: props.keyboardType,
}))`
  flex: 1;
  margin-top: ${Platform.OS === 'ios' ? 0 : -12}px;
  padding-left: 10px;
  font-size: 18px;
  color: ${props => props.theme.text};
`;

const ButtonsBox = styled(Centered)`
  margin-top: 20px;
`;
