import React, {useState, useCallback, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {
  Avatar,
  Drawer,
  Text,
  TouchableRipple,
  Switch,
} from 'react-native-paper';
import {
  DrawerContentScrollView,
  DrawerItem,
  useIsDrawerOpen,
} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';

import {AuthContext, Fire} from '../components';
import {ROUTES, IMAGES, FONTS} from '../shared/constants';
import {switchTheme} from '../redux';
import {lightTheme, darkTheme, altTheme} from './../components/Themes';
import {selectTheme} from './../redux/reducers/ThemeSlice';

export function DrawerContent(props, {navigation}) {
  const user = useSelector(state => state.auth.currentUser);
  const theme = useSelector(selectTheme);
  const dispatch = useDispatch();
  const isDrawerOpened = useIsDrawerOpen();
  const [isThemeSwitchOn, setIsThemeSwitchOn] = React.useState(theme.state);

  // Switch theme
  const onToggleSwitch = () => {
    setIsThemeSwitchOn(!isThemeSwitchOn);
    isThemeSwitchOn === false
      ? dispatch(switchTheme(darkTheme))
      : dispatch(switchTheme(lightTheme));
  };

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <DrawerContentScrollView>
          <View style={styles.drawerContent}>
            <View style={styles.userInfoSection}>
              {user.map(item => (
                <View>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 10,
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <Avatar.Image
                      source={
                        item.avatar ? {uri: item.avatar} : IMAGES.tempAvatar
                      }
                      size={70}
                    />
                    <View
                      style={{
                        flexDirection: 'column',
                        marginHorizontal: 4,
                      }}>
                      <UserName style={{...FONTS.h3}}>
                        {item.displayName ? item.displayName : 'No name'}
                      </UserName>
                      <UserEmail style={{...FONTS.body4}}>
                        {item.email ? item.email : 'No email'}
                      </UserEmail>
                    </View>
                  </View>
                </View>
              ))}
            </View>

            <Drawer.Section style={styles.drawerSection}>
              <DrawerItem
                icon={({size}) => (
                  <Icon name="home" color={theme.text} size={size} />
                )}
                label="Home"
                labelStyle={{color: theme.text, ...FONTS.body4}}
                onPress={() => {
                  props.navigation.navigate(ROUTES.home);
                }}
              />
              <DrawerItem
                icon={({size}) => (
                  <Icon name="grid" color={theme.text} size={size} />
                )}
                label="Explore"
                labelStyle={{color: theme.text, ...FONTS.body4}}
                onPress={() => {
                  props.navigation.navigate(ROUTES.explore);
                }}
              />
              <DrawerItem
                icon={({size}) => (
                  <Icon name="user" color={theme.text} size={size} />
                )}
                label="Profile"
                labelStyle={{color: theme.text, ...FONTS.body4}}
                onPress={() => {
                  props.navigation.navigate(ROUTES.profile);
                }}
              />
              <DrawerItem
                icon={({size}) => (
                  <Icon name="settings" color={theme.text} size={size} />
                )}
                label="Settings"
                labelStyle={{color: theme.text, ...FONTS.body4}}
                onPress={() => {
                  props.navigation.navigate(ROUTES.settings);
                }}
              />
              <DrawerItem
                icon={({size}) => (
                  <Icon name="shopping-cart" color={theme.text} size={size} />
                )}
                label="Cart"
                labelStyle={{color: theme.text, ...FONTS.body4}}
                onPress={() => {
                  props.navigation.navigate(ROUTES.cart);
                }}
              />
              <DrawerItem
                icon={({size}) => (
                  <Icon name="shopping-bag" color={theme.text} size={size} />
                )}
                label="Checkout"
                labelStyle={{color: theme.text, ...FONTS.body4}}
                onPress={() => {
                  props.navigation.navigate(ROUTES.checkout);
                }}
              />
              <DrawerItem
                icon={({size}) => (
                  <Icon name="folder" color={theme.text} size={size} />
                )}
                label="Transactions"
                labelStyle={{color: theme.text, ...FONTS.body4}}
                onPress={() => {
                  props.navigation.navigate(ROUTES.transactions);
                }}
              />
            </Drawer.Section>

            {/* PREFERENCES */}
            <Drawer.Section>
              <Preferences style={{...FONTS.h3}}>Preferences</Preferences>
              <View style={styles.preference}>
                <Text style={{color: theme.text, ...FONTS.body4}}>
                  Dark Theme
                </Text>
                <View>
                  <Switch
                    value={isThemeSwitchOn || theme.state}
                    onValueChange={onToggleSwitch}
                  />
                </View>
              </View>
            </Drawer.Section>
          </View>
        </DrawerContentScrollView>
        <Drawer.Section style={styles.bottomDrawerSection}>
          <DrawerItem
            icon={({size}) => (
              <Icon name="log-out" color={theme.text} size={size} />
            )}
            label="Sign Out"
            labelStyle={{color: theme.text, ...FONTS.body4}}
            onPress={() => {
              Fire.shared.signOut();
            }}
          />
        </Drawer.Section>
      </Container>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 8,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const UserName = styled.Text`
  color: ${props => props.theme.text};
  margin-top: 3px;
`;

const UserEmail = styled.Text`
  color: ${props => props.theme.text};
  font-size: 14px;
  line-height: 14px;
  overflow: hidden;
`;

const Preferences = styled.Text`
  color: ${props => props.theme.text};
  margin-top: 3px;
  margin-left: 15px;
`;
