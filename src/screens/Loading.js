/* 
eslint-disable react/prop-types 
*/
import React from 'react';
import {Spinner} from '../components';

const Loading = ({}) => {
  return <Spinner />;
};

export default Loading;
