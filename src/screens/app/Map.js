import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';

import {IMAGES, COLORS, SIZES, FONTS} from './../../shared';
import {selectTheme} from './../../redux/reducers/ThemeSlice';

const {width, height} = Dimensions.get('screen');

export default function map() {
  const theme = useSelector(selectTheme);
  return (
    <ThemeProvider theme={theme}>
      <Container>
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={{
            flex: 1,
            width: width * 1,
            justifyContent: 'flex-end',
            alignItems: 'center',
            backgroundColor: theme.background,
          }}
          loadingBackgroundColor={theme.background}
          region={{
            latitude: -15.40669,
            longitude: 28.28713,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}></MapView>
      </Container>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
  justify-content: flex-end;
  align-items: center;
`;
