import React, {useState, useEffect, useLayoutEffect} from 'react';
import {
  View,
  StatusBar,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation, useRoute} from '@react-navigation/native';

import {store} from './../../redux/Store';
import {IMAGES, FONTS, ROUTES} from './../../shared';
import {selectTheme} from './../../redux/reducers/ThemeSlice';
import {selectProducts} from './../../redux/reducers/ProductSlice';

const {width, height} = Dimensions.get('screen');

const VendorInfo = () => {
  const theme = useSelector(selectTheme);
  const {vendorData, products} = useSelector(selectProducts);
  const currentVendorID = useSelector(state => state.prodSlice.vendorID);
  const navigation = useNavigation();
  const numberOfProducts = getNumber();
  const numberSold = productsSold();
  const prodsInStock = inStock();

  function getNumber() {
    const items = products.filter(({vendorID}) => vendorID === currentVendorID);
    return items.length;
  }

  function productsSold() {
    const soldItems = products.filter(
      ({vendorID, isSold}) => vendorID === currentVendorID && isSold === true,
    );
    return soldItems.length;
  }

  function inStock() {
    let stock = parseInt(numberOfProducts) - parseInt(numberSold);
    return stock;
  }

  function renderProfile(item, index) {
    return (
      <View>
        <View>
          <TouchableOpacity style={styles.avatarContainer}>
            <Image
              source={item.avatar ? {uri: item.avatar} : IMAGES.tempAvatar}
              resizeMethod="auto"
              style={styles.avatar}
            />
          </TouchableOpacity>

          {/* Top stats */}
          <View style={styles.statsContainer}>
            <View style={styles.stat}>
              <Text
                style={[
                  styles.statAmount,
                  {color: theme.text, ...FONTS.body3},
                ]}>
                {numberOfProducts}
              </Text>
              <Text
                style={[styles.statTitle, {color: theme.accent, ...FONTS.h4}]}>
                Products
              </Text>
            </View>
            <View style={styles.stat}>
              <Text
                style={[
                  styles.statAmount,
                  {color: theme.text, ...FONTS.body3},
                ]}>
                {numberSold}
              </Text>
              <Text
                style={[styles.statTitle, {color: theme.accent, ...FONTS.h4}]}>
                Sold
              </Text>
            </View>
            <View style={styles.stat}>
              <Text
                style={[
                  styles.statAmount,
                  {color: theme.text, ...FONTS.body3},
                ]}>
                {prodsInStock}
              </Text>
              <Text
                style={[styles.statTitle, {color: theme.accent, ...FONTS.h4}]}>
                In Stock
              </Text>
            </View>
          </View>

          {/* Profile info */}
          <View style={styles.userContainer}>
            <View style={styles.user}>
              <Text
                style={[styles.userTitle, {color: theme.accent, ...FONTS.h3}]}>
                Name
              </Text>
              <Text
                style={[styles.userValue, {color: theme.text, ...FONTS.body2}]}>
                {item.displayName ? item.displayName : 'No name'}
              </Text>
            </View>
            <View style={styles.user}>
              <Text
                style={[styles.userTitle, {color: theme.accent, ...FONTS.h3}]}>
                Email
              </Text>
              <Text
                style={[styles.userValue, {color: theme.text, ...FONTS.body2}]}>
                {item.email ? item.email : 'No email'}
              </Text>
            </View>
            <View style={styles.user}>
              <Text
                style={[styles.userTitle, {color: theme.accent, ...FONTS.h3}]}>
                Phone
              </Text>
              <Text
                style={[styles.userValue, {color: theme.text, ...FONTS.body2}]}>
                {item.phoneNumber ? item.phoneNumber : 'No phone number'}
              </Text>
            </View>
            <View style={styles.user}>
              <Text
                style={[styles.userTitle, {color: theme.accent, ...FONTS.h3}]}>
                About Us
              </Text>
              <Text
                style={[styles.userValue, {color: theme.text, ...FONTS.body2}]}>
                {item.aboutUs ? item.aboutUs : 'No info'}
              </Text>
            </View>
            <View style={styles.user}>
              <Text
                style={[styles.userTitle, {color: theme.accent, ...FONTS.h3}]}>
                Address
              </Text>
              <TouchableOpacity>
                <Text
                  style={[
                    styles.userValue,
                    {color: theme.secondary, ...FONTS.body2},
                  ]}>
                  {item.address ? item.address : 'No info'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />
        <FlatList
          data={vendorData}
          keyExtractor={item => item.key}
          renderItem={({item, index}) => renderProfile(item, index)}
          horizontal={false}
        />
      </Container>
    </ThemeProvider>
  );
};

export default VendorInfo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  editbtn: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 45,
    height: 45,
    borderRadius: 25,
  },
  btn: {
    backgroundColor: 'crimson',
    fontSize: 20,
  },
  userContainer: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginHorizontal: 12,
  },
  user: {
    alignItems: 'flex-start',
  },
  userValue: {
    fontWeight: '300',
  },
  userTitle: {
    fontWeight: '500',
    marginTop: 5,
    paddingTop: 8,
  },
  avatarContainer: {
    shadowColor: '#151734',
    shadowRadius: 30,
    shadowOpacity: 0.4,
    alignItems: 'center',
    marginVertical: 10,
  },
  avatar: {
    width: 136,
    height: 136,
    borderRadius: 68,
  },
  statsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 32,
    marginVertical: 10,
  },
  stat: {
    alignItems: 'center',
    flex: 1,
  },
  statAmount: {
    fontWeight: '300',
  },
  statTitle: {
    fontSize: 12,
    fontWeight: '500',
    marginTop: 4,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ScrollContainer = styled.ScrollView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Text = styled.Text`
  font-size: 18px;
  font-weight: 500;
  color: ${props => props.theme.text};
`;
