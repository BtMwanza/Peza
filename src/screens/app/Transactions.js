import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import {useNavigation} from '@react-navigation/native';
import {Button} from 'react-native-paper';

import {SIZES, FONTS, ROUTES, BUTTONS} from '../../shared';
import {selectTheme} from '../../redux/reducers/ThemeSlice';
import {selectCart} from '../../redux/reducers/CartSlice';
import {filterList, fetchData} from '../../redux';
import {selectProducts} from './../../redux/reducers/ProductSlice';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const Transactions = () => {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  return (
    <ThemeProvider theme={theme}>
      <StatusBar
        backgroundColor={theme.background}
        barStyle={theme.status_bar}
      />
      <ScrollContainer>
        <View>
          <HeaderContainer>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{color: theme.text, ...FONTS.h4}}>TXN_ID</Text>
              <Text style={{color: theme.text, ...FONTS.h4}}>Date</Text>
            </View>
          </HeaderContainer>
        </View>
      </ScrollContainer>
    </ThemeProvider>
  );
};

export default Transactions;

const styles = StyleSheet.create({});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ScrollContainer = styled.ScrollView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

const ButtonsBox = styled(Centered)`
  margin-top: 40px;
`;

const ProductName = styled.Text`
  font-weight: 900;
  flex-wrap: wrap;
  font-size: ${SIZES.h4}px;
  margin-top: ${SIZES.radius}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ProductPrice = styled.Text`
  font-size: ${SIZES.h5}px;
  margin-top: ${SIZES.radius}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ItemsContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding-vertical: 4px;
  max-width: 100%;
`;

const HeaderContainer = styled.View`
  padding-horizontal: 10px;
  max-width: 100%;
`;
