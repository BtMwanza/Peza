import React, {useEffect, useState} from 'react';
import {View, Text, Dimensions, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {Button} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import styled, {ThemeProvider} from 'styled-components';

import {SIZES, FONTS, BUTTONS, ROUTES} from '../../shared';
import {selectTheme} from '../../redux/reducers/ThemeSlice';
import {selectCart} from '../../redux/reducers/CartSlice';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const Summary = () => {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const dispatch = useDispatch();
  const totalPrice = getTotal();
  const navigation = useNavigation();

  function getTotal() {
    return cart
      .reduce((acc, item) => {
        return acc + item.price;
      }, 0)
      .toFixed(2);
  }

  return (
    <ThemeProvider theme={theme}>
      <ScrollContainer>
        <View>
          <HeaderContainer>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style={{color: theme.text, ...FONTS.h4}}>Cart Total</Text>
              <Text style={{color: theme.text, ...FONTS.h4}}>
                K{totalPrice}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: 10,
              }}>
              <Text style={{color: theme.text, ...FONTS.h4}}>Delivery</Text>
              <Text style={{color: theme.text, ...FONTS.h4}}>K0.00</Text>
            </View>
          </HeaderContainer>

          <View style={{paddingHorizontal: 10, marginVertical: 5}}>
            <Text style={{color: theme.text, ...FONTS.h4}}>
              {cart.length} Items in cart
            </Text>
          </View>
          <View style={{marginVertical: 10}}>
            {cart.map((item, index) => {
              return (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    width: '100%',
                  }}>
                  {/* Right side components */}
                  <View style={{flex: 1, width: '100%'}}>
                    <ItemsContainer>
                      {/* Left Side */}
                      <ProductName style={{...FONTS.body5}}>
                        {item.productName}
                      </ProductName>

                      {/* Right Side */}
                      <ProductPrice style={{...FONTS.body5}}>
                        K{item.price.toFixed(2)}
                      </ProductPrice>
                    </ItemsContainer>
                  </View>
                </View>
              );
            })}
          </View>
          <ButtonsBox>
            <Button
              style={{borderColor: theme.text}}
              color={theme.text}
              mode="outlined"
              onPress={() => {
                navigation.navigate(ROUTES.payment);
              }}>
              {BUTTONS.proceed}
            </Button>
          </ButtonsBox>
        </View>
      </ScrollContainer>
    </ThemeProvider>
  );
};

export default Summary;

const styles = StyleSheet.create({
  title: {
    ...FONTS.body3,
    fontSize: 20,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ScrollContainer = styled.ScrollView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

const ButtonsBox = styled(Centered)`
  margin-top: 40px;
`;

const ProductName = styled.Text`
  font-weight: 900;
  flex-wrap: wrap;
  font-size: ${SIZES.h4}px;
  margin-top: ${SIZES.radius}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ProductPrice = styled.Text`
  font-size: ${SIZES.h5}px;
  margin-top: ${SIZES.radius}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ItemsContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding-vertical: 4px;
  max-width: 100%;
`;

const HeaderContainer = styled.View`
  padding-horizontal: 10px;
  max-width: 100%;
`;
