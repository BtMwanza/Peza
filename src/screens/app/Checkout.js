import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  StatusBar,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {Avatar, Button, Card, Title, Paragraph} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styled, {ThemeProvider} from 'styled-components';
import StepIndicator from 'react-native-step-indicator';
import Swiper from 'react-native-swiper';
import * as Animatable from 'react-native-animatable';
import {PayWithFlutterwave, FlutterwaveInit} from 'flutterwave-react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import shorthash from 'shorthash';
import moment from 'moment';

import {
  IMAGES,
  ROUTES,
  SIZES,
  FONTS,
  PLACEHOLDERS,
  BUTTONS,
} from './../../shared';
import {selectTheme} from './../../redux/reducers/ThemeSlice';
import {selectCart} from './../../redux/reducers/CartSlice';
import {addToPayload} from './../../redux';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default function Checkout() {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const totalPrice = getTotal();

  const [data, setData] = useState({
    fullname: '',
    email: '',
    address: '',
    phoneNumber: '',
    check_nameChange: false,
    check_emailChange: false,
    check_addressChange: false,
    check_phoneChange: false,
  });

  const nameChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        fullname: val,
        check_nameChange: true,
      });
    } else {
      setData({
        ...data,
        fullname: val,
        check_nameChange: false,
      });
    }
  };

  const emailChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        email: val,
        check_emailChange: true,
      });
    } else {
      setData({
        ...data,
        email: val,
        check_emailChange: false,
      });
    }
  };

  const addressChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        address: val,
        check_addressChange: true,
      });
    } else {
      setData({
        ...data,
        address: val,
        check_addressChange: false,
      });
    }
  };

  const phoneChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        phoneNumber: val,
        check_phoneChange: true,
      });
    } else {
      setData({
        ...data,
        phoneNumber: val,
        check_phoneChange: false,
      });
    }
  };

  function getTotal() {
    return cart
      .reduce((acc, item) => {
        return acc + item.price;
      }, 0)
      .toFixed(2);
  }

  const payload = {
    tx_ref: 'TX' + '_' + shorthash.random(16),
    amount: parseFloat(totalPrice),
    currency: 'ZMW',
    payment_options: 'mobilemoney',
    customer: {
      email: data.email,
      phoneNumber: data.phoneNumber,
      name: data.fullname,
    },
  };

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />
        {/* BILLING */}
        <SwipeContainer>
          <View>
            <View style={styles.action}>
              <Fontisto name="person" color={theme.text} size={20} />
              <TextInput
                placeholder={PLACEHOLDERS.fullname}
                placeholderTextColor="gray"
                style={[styles.textInput, {color: theme.text}]}
                autoCapitalize="none"
                onChangeText={val => nameChange(val)}
              />
              {data.check_nameChange ? (
                <Animatable.View animation="bounceIn">
                  <Feather
                    name="check-circle"
                    color={theme.onBackground}
                    size={20}
                  />
                </Animatable.View>
              ) : null}
            </View>

            {/* Email */}
            <View style={styles.action}>
              <Fontisto name="email" color={theme.text} size={20} />
              <TextInput
                placeholder={PLACEHOLDERS.email}
                placeholderTextColor="gray"
                style={[styles.textInput, {color: theme.text}]}
                autoCapitalize="none"
                onChangeText={val => emailChange(val)}
              />
              {data.check_emailChange ? (
                <Animatable.View animation="bounceIn">
                  <Feather
                    name="check-circle"
                    color={theme.onBackground}
                    size={20}
                  />
                </Animatable.View>
              ) : null}
            </View>

            {/* Phone number */}
            <View style={styles.action}>
              <Fontisto name="phone" color={theme.text} size={20} />
              <TextInput
                placeholder={PLACEHOLDERS.phone}
                placeholderTextColor="gray"
                style={[styles.textInput, {color: theme.text}]}
                autoCapitalize="none"
                onChangeText={val => phoneChange(val)}
              />
              {data.check_phoneChange ? (
                <Animatable.View animation="bounceIn">
                  <Feather
                    name="check-circle"
                    color={theme.onBackground}
                    size={20}
                  />
                </Animatable.View>
              ) : null}
            </View>

            {/* Street address */}
            <View style={styles.action}>
              <Fontisto name="map-marker-alt" color={theme.text} size={20} />
              <TextInput
                placeholder={PLACEHOLDERS.address}
                placeholderTextColor="gray"
                style={[styles.textInput, {color: theme.text}]}
                autoCapitalize="none"
                onChangeText={val => addressChange(val)}
              />
              {data.check_addressChange ? (
                <Animatable.View animation="bounceIn">
                  <Feather
                    name="check-circle"
                    color={theme.onBackground}
                    size={20}
                  />
                </Animatable.View>
              ) : null}
            </View>

            <ButtonsBox>
              <Button
                style={{borderColor: theme.text, marginVertical: 20}}
                color={theme.text}
                mode="outlined"
                onPress={() => {
                  dispatch(addToPayload(payload));
                  navigation.navigate(ROUTES.summary);
                }}>
                {BUTTONS.proceed}
              </Button>
            </ButtonsBox>
          </View>
        </SwipeContainer>
      </Container>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: Platform.OS === 'ios' ? 3 : 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 0,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
    fontSize: 18,
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
  },
  color_textPrivate: {
    color: 'grey',
  },
  errorMsg: {
    fontSize: 14,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ScrollContainer = styled.ScrollView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

const ButtonsBox = styled(Centered)`
  margin-top: 40px;
`;

const SwipeContainer = styled.View`
  padding-horizontal: 5px;
  padding-vertical: 5px;
`;

const ProductName = styled.Text`
  font-weight: 900;
  flex-wrap: wrap;
  font-size: ${SIZES.h4}px;
  margin-top: ${SIZES.radius}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ProductPrice = styled.Text`
  font-size: ${SIZES.h5}px;
  margin-top: ${SIZES.radius}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ItemsContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding-vertical: 4px;
  max-width: 100%;
`;

const HeaderContainer = styled.View`
  padding-horizontal: 10px;
  max-width: 100%;
`;
