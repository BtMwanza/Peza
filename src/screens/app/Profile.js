import React, {useState, useEffect, useLayoutEffect} from 'react';
import {
  View,
  StatusBar,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import Icon from 'react-native-vector-icons/Feather';
import {Button} from 'react-native-paper';
import {useNavigation, useRoute} from '@react-navigation/native';

import {IMAGES, FONTS, ROUTES} from './../../shared';
import {selectTheme} from './../../redux/reducers/ThemeSlice';
import {selectAuth} from './../../redux/reducers/AuthSlice';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const Profile = () => {
  const theme = useSelector(selectTheme);
  const {currentUser} = useSelector(selectAuth);
  const navigation = useNavigation();

  function renderProfile(item, index) {
    return (
      <View key={item => item.uid}>
        <View>
          <TouchableOpacity style={styles.avatarContainer}>
            <Image
              source={item.avatar ? {uri: item.avatar} : IMAGES.tempAvatar}
              resizeMethod="auto"
              style={styles.avatar}
            />
          </TouchableOpacity>

          <View style={styles.statsContainer}>
            <TouchableOpacity style={styles.stat}>
              <Text style={[styles.statAmount, {...FONTS.body3}]}>0</Text>
              <Text
                style={[styles.statTitle, {color: theme.accent, ...FONTS.h4}]}>
                Orders
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.userContainer}>
            <View style={styles.user}>
              <Text
                style={[styles.userTitle, {color: theme.accent, ...FONTS.h3}]}>
                Name
              </Text>
              <Text style={[styles.userValue, {...FONTS.body2}]}>
                {item.displayName ? item.displayName : 'No name'}
              </Text>
            </View>

            <View style={styles.user}>
              <Text
                style={[styles.userTitle, {color: theme.accent, ...FONTS.h3}]}>
                Email
              </Text>
              <Text style={[styles.userValue, {...FONTS.body2}]}>
                {item.email ? item.email : 'No email'}
              </Text>
            </View>

            <View style={styles.user}>
              <Text
                style={[styles.userTitle, {color: theme.accent, ...FONTS.h3}]}>
                Phone number
              </Text>
              <Text style={[styles.userValue, {...FONTS.body2}]}>
                {item.phoneNumber ? item.phoneNumber : 'No email'}
              </Text>
            </View>
          </View>

          <ButtonsBox>
            <Button
              style={{borderColor: theme.text}}
              color={theme.text}
              mode="outlined"
              onPress={() => {
                navigation.navigate(ROUTES.editProfile);
              }}>
              Update
            </Button>
          </ButtonsBox>
        </View>
      </View>
    );
  }

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />
        <FlatList
          data={currentUser}
          keyExtractor={item => item.key}
          renderItem={({item, index}) => renderProfile(item, index)}
          horizontal={false}
        />
      </Container>
    </ThemeProvider>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  editbtn: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 45,
    height: 45,
    borderRadius: 25,
  },
  btn: {
    backgroundColor: 'crimson',
    fontSize: 20,
  },
  userContainer: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginHorizontal: 12,
  },
  user: {
    alignItems: 'flex-start',
  },
  userValue: {
    fontSize: 18,
    fontWeight: '300',
  },
  userTitle: {
    fontSize: 15,
    fontWeight: '500',
    marginTop: 10,
    paddingTop: 8,
  },
  avatarContainer: {
    shadowColor: '#151734',
    shadowRadius: 68,
    shadowOpacity: 0.6,
    alignItems: 'center',
    marginVertical: 10,
    elevation: 3,
  },
  avatar: {
    width: 136,
    height: 136,
    borderRadius: 68,
  },
  statsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: 32,
    marginVertical: 10,
  },
  stat: {
    alignItems: 'center',
    flex: 1,
  },
  statAmount: {
    fontWeight: '300',
  },
  statTitle: {
    fontSize: 12,
    fontWeight: '500',
    marginTop: 4,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ScrollContainer = styled.ScrollView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Text = styled.Text`
  font-size: 18px;
  font-weight: 500;
  color: ${props => props.theme.text};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

const ButtonsBox = styled(Centered)`
  margin-top: 10px;
`;
