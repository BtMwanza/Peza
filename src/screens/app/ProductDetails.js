import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  Dimensions,
  FlatList,
} from 'react-native';
import {Button} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import {useRoute, useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Feather';
import firebase from 'firebase/app';

import {IMAGES, COLORS, SIZES, FONTS, ROUTES, BUTTONS} from '../../shared';
import {selectTheme} from '../../redux/reducers/ThemeSlice';
import {setVendorID, addItem, deleteItem} from '../../redux';
import {selectProducts} from '../../redux/reducers/ProductSlice';
import {selectCart} from '../../redux/reducers/CartSlice';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default function ProductDetails() {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const {currentProduct, viewed} = useSelector(selectProducts);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // Function to add product to the cart
  const addToCart = item => {
    try {
      dispatch(addItem(item));
    } catch (error) {
      alert(error);
    }
  };

  // Function to remove product to the cart
  const removeFromCart = item => {
    let id = item.productID;
    try {
      dispatch(deleteItem(id));
    } catch (error) {
      alert(error);
    }
  };

  const toggleCart = item => {
    try {
      const added = cart.find(p => p.productID === item.productID);
      if (!added) {
        addToCart(item);
      } else {
        removeFromCart(item);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // Send viewed product to firebase
  function saveViewed(params) {
    let view = {...viewed};
    firebase
      .firestore()
      .collection('PRODUCTS')
      .doc('VIEWEDPRODUCTS')
      .set({view});
    console.log('PROD_ID: ', view);
  }

  React.useEffect(() => {
    saveViewed();
    return () => {};
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />
        <InnerBox key={currentProduct.productID}>
          <TitleBox>
            <Text style={{color: theme.onSurface, ...FONTS.h1}}>
              {currentProduct.productName}
            </Text>
          </TitleBox>

          <LikeBox>
            <Icon
              name={'heart'}
              color={theme.text}
              style={{backgroundColor: theme.background}}
              size={25}
            />
          </LikeBox>
          <ImageBox>
            <Image
              style={{height: 200, width: 300, resizeMode: 'contain'}}
              source={{uri: currentProduct.image}}
            />
          </ImageBox>

          {/* Top stats */}
          <StatContainer>
            <Stat>
              <StatTitle style={{color: theme.accent, ...FONTS.h4}}>
                Make
              </StatTitle>
              <StatValue style={{color: theme.text, ...FONTS.body3}}>
                {currentProduct.make}
              </StatValue>
            </Stat>
            <Stat>
              <StatTitle style={{color: theme.accent, ...FONTS.h4}}>
                Model
              </StatTitle>
              <StatValue style={{color: theme.text, ...FONTS.body3}}>
                {currentProduct.model}
              </StatValue>
            </Stat>
            <Stat>
              <StatTitle style={{color: theme.accent, ...FONTS.h4}}>
                Year
              </StatTitle>
              <StatValue style={{color: theme.text, ...FONTS.body3}}>
                {currentProduct.year}
              </StatValue>
            </Stat>
          </StatContainer>

          <DetailsBox>
            <Text style={{color: theme.onSurface, ...FONTS.body3}}>
              {currentProduct.desc}
            </Text>

            <Heading style={{...FONTS.h3}}>Brand</Heading>
            <Text style={{color: theme.onSurface, ...FONTS.body3}}>
              {currentProduct.brand}
            </Text>

            <Heading style={{...FONTS.h3}}>Extra Info</Heading>
            <Text style={{color: theme.onSurface, ...FONTS.body3}}>
              {currentProduct.extraInfo}
            </Text>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text style={{color: theme.onSurface, ...FONTS.h2}}>
                K{currentProduct.price}
              </Text>
              <View>
                <Button
                  style={{borderColor: theme.text}}
                  color={theme.text}
                  mode="outlined"
                  onPress={() => {
                    toggleCart(currentProduct);
                  }}>
                  {BUTTONS.cart}
                </Button>
              </View>
            </View>
          </DetailsBox>
        </InnerBox>

        <PurchaseBox>
          <Button
            style={{borderColor: theme.text, marginVertical: 15}}
            color={theme.secondary}
            mode="contained"
            onPress={() => {
              navigation.navigate(ROUTES.vendor);
            }}>
            View vendor
          </Button>
        </PurchaseBox>
      </Container>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  statsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: 32,
    marginVertical: 10,
  },
  stat: {
    alignItems: 'center',
    flex: 1,
  },
  statAmount: {
    fontWeight: '300',
  },
  statTitle: {
    fontSize: 12,
    fontWeight: '500',
    marginTop: 4,
  },
});

const StatContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-horizontal: 32px;
  margin-vertical: 10px;
`;

const Stat = styled.View`
  flex: 1;
  align-items: center;
`;

const StatValue = styled.Text`
  font-weight: 300;
`;
const StatTitle = styled.Text`
  font-size: 12px;
  font-weight: 500;
  margin-top: 4px;
`;

// STYLED COMPONENTS
const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const InnerBox = styled.ScrollView`
  padding: 5px;
`;

const ImageBox = styled.View`
  align-items: center;
  justify-content: center;
  margin-top: 10px;
`;

const DetailsBox = styled.View`
  padding: 20px;
`;

const TitleBox = styled.View`
  padding: 20px;
  align-items: center;
  justify-content: center;
`;

const PurchaseBox = styled.View`
  align-items: center;
  justify-content: center;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  background-color: ${props => props.theme.surface};
`;

const LikeBox = styled.TouchableOpacity`
  aspect-ratio: 1;
  background-color: ${props => props.theme.surface};
  position: absolute;
  top: 5px;
  right: 10px;
  justify-content: center;
  align-items: center;
`;

const Heading = styled.Text`
  margin-top: 10px;
  color: ${props => props.theme.onSurface};
`;
