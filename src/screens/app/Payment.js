import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {Button} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/Feather';
import styled, {ThemeProvider} from 'styled-components';
import {WebView} from 'react-native-webview';
import {PayWithFlutterwave, FlutterwaveInit} from 'flutterwave-react-native';

import AddSubscription from './AddSubscription';
import {IMAGES, SIZES, FONTS, PLACEHOLDERS, BUTTONS} from '../../shared';
import {selectTheme} from '../../redux/reducers/ThemeSlice';
import {selectCart} from '../../redux/reducers/CartSlice';
import {
  PUBLIC_KEY,
  SECRET_KEY,
} from '../../components/paymentGateway/Flutterwave';

const {width} = Dimensions.get('screen');

const Payment = () => {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const payload = useSelector(selectCart);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  handleOnRedirect = () => {};

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <AddSubscription />
      </Container>
    </ThemeProvider>
  );
};

export default Payment;

const styles = StyleSheet.create({});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

const ButtonsBox = styled(Centered)`
  margin-top: 40px;
`;
