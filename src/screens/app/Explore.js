import React from 'react';
import {
  View,
  StatusBar,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import Icon from 'react-native-vector-icons/MaterialIcons';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Svg, Polygon} from 'react-native-svg';
import MasonryList from '@react-native-seoul/masonry-list';
import {useNavigation} from '@react-navigation/native';

import {IMAGES, SIZES, FONTS, ROUTES} from './../../shared';
import {selectTheme} from './../../redux/reducers/ThemeSlice';
import {LimitedDealsSection, RecentProducts, Popular} from './../../components';
import {selectCart} from './../../redux/reducers/CartSlice';
import {filterList, searchList, addItem, deleteItem} from './../../redux';
import {selectProducts} from './../../redux/reducers/ProductSlice';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const Explore = () => {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const {products} = useSelector(selectProducts);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // Dummy Data
  const [data, setData] = React.useState({
    loading: false,
    productData: [],
    search: '',
    category: [
      {
        id: 0,
        icon: IMAGES.engineIcon,
        category: 'Engine & Emissions',
      },
      {
        id: 1,
        icon: IMAGES.headlightIcon,
        category: 'Lighting',
      },
      {
        id: 2,
        icon: IMAGES.brakesIcon,
        category: 'Brakes & Suspension',
      },
      {
        id: 3,
        icon: IMAGES.tyreIcon,
        category: 'Tyres & Rims',
      },
      {
        id: 4,
        icon: IMAGES.wireIcon,
        category: 'Wiring',
      },
      {
        id: 5,
        icon: IMAGES.batteryIcon,
        category: 'Electrical',
      },
    ],
  });

  const searchFilter = text => {
    if (text) {
      const newData = data.productData.filter(item => {
        const itemData = item.productName
          ? item.productName.toUpperCase()
          : ''.toLowerCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setData({...data, productData: newData, search: text});
    } else {
      setData({...data, productData: products, search: text});
    }
  };

  // Function to add product to the cart
  const addToCart = item => {
    try {
      dispatch(addItem(item));
      console.log('Current Cart: ', cart);
    } catch (error) {
      alert(error);
    }
  };

  // Function to remove product to the cart
  const removeFromCart = item => {
    let id = item.productID;
    try {
      dispatch(deleteItem(id));
    } catch (error) {
      alert(error);
    }
  };

  const toggleCart = item => {
    try {
      const added = cart.find(p => p.productID === item.productID);
      if (!added) {
        addToCart(item);
      } else {
        removeFromCart(item);
      }
    } catch (error) {
      console.log(error);
    }
  };

  function renderData(item, index) {
    const randomBool = React.useMemo(() => Math.random() < 0.35, []);
    return (
      <ProductBox style={[styles.shadow, {height: randomBool ? 60 : 80}]}>
        <Image source={item.icon} style={styles.icon} />
        <Text
          style={[
            styles.title,
            {
              color: theme.text,
            },
          ]}>
          {item.category}
        </Text>
      </ProductBox>
    );
  }

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />

        <ScrollView>
          <View>
            <Heading
              style={{
                ...FONTS.navTitle,
              }}>
              Discover New Things
            </Heading>
          </View>

          <SearchBox>
            <SearchInput
              style={{color: theme.text, ...FONTS.body4}}
              value={data.search}
              placeholder="Search..."
              placeholderTextColor={theme.text}
              /* onChangeText={val => textInputChange(val)} */
              onChangeText={text => searchFilter(text)}
            />

            <SearchButton>
              <Icon name="search" color={theme.text} size={25} />
            </SearchButton>
          </SearchBox>
          <View>
            <MasonryList
              columnWrapperStyle={{
                justifyContent: 'space-between',
              }}
              contentContainerStyle={{
                paddingTop: 5,
                paddingHorizontal: SIZES.radius,
                justifyContent: 'center',
                alignItem: 'center',
              }}
              data={data.category}
              keyExtractor={(item, index) => item.id}
              numColumns={4}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => renderData(item, index)}
              horizontal={false}
            />
          </View>

          <View>
            <Popular />
            {/* PRODUCTS */}
            <RecentProducts />

            <LimitedDealsSection />
          </View>
        </ScrollView>
      </Container>
    </ThemeProvider>
  );
};

export default Explore;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3.84,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    height: 25,
    width: 25,
    marginVertical: 2,
  },
  title: {
    marginVertical: 1,
    fontSize: 12,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Heading = styled.Text`
  margin-top: ${SIZES.base}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ImportBox = styled.View`
  flex: 25;
  background-color: ${props => props.theme.accent};
  border-top-right-radius: 25px;
  border-top-left-radius: 25px;
`;

const ProductBox = styled.TouchableOpacity`
  flex: 1;
  background-color: ${props => props.theme.surface};
  margin: 0px 10px 10px 5px;
  align-items: center;
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
`;

const SearchBox = styled.View``;

const SearchInput = styled.TextInput`
  height: 40px;
  border-width: 1px;
  padding: 5px 10px 5px 10px;
  margin: 5px;
  border-color: #009688;
  border-top-right-radius: 25px;
  border-top-left-radius: 25px;
  border-bottom-left-radius: 25px;
  border-bottom-right-radius: 25px;
`;

const SearchButton = styled.View`
  position: absolute;
  top: 10px;
  right: 20px;
`;

const PopularBox = styled.View`
  flex: 1;
  margin: 0px 10px 10px 5px;
  align-items: center;
`;

const PopularCircle = styled.TouchableOpacity`
  flex: 1;
  height: 50px;
  width: 100px;
  background-color: ${props => props.theme.surface};
  align-items: center;
  border-radius: 50px;
`;
