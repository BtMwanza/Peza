import React from 'react';
import {View, StatusBar, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import {TouchableRipple, Switch, List} from 'react-native-paper';

import {IMAGES, COLORS, SIZES, FONTS} from './../../shared';
import {switchTheme} from '../../redux';
import {lightTheme, darkTheme} from '../../components';
import {selectTheme} from './../../redux/reducers/ThemeSlice';

const Settings = ({navigation}) => {
  const theme = useSelector(selectTheme);
  const dispatch = useDispatch();
  const [isNotificationOn, setIsNotificationOn] = React.useState(false);
  const [isThemeSwitchOn, setIsThemeSwitchOn] = React.useState(theme.state);

  const onToggleNotification = () => setIsNotificationOn(!isNotificationOn);

  const onToggleTheme = () => {
    setIsThemeSwitchOn(!isThemeSwitchOn);
    isThemeSwitchOn === false
      ? dispatch(switchTheme(darkTheme))
      : dispatch(switchTheme(lightTheme));
  };

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />
        <View style={{paddingHorizontal: 5, paddingVertical: 10}}>
          <List.Item
            title="Notification"
            titleStyle={{color: theme.text, ...FONTS.h4}}
            description="Toggle push notification"
            descriptionStyle={{color: theme.text, ...FONTS.body5}}
            right={props => (
              <View>
                <Switch
                  value={isNotificationOn}
                  onValueChange={onToggleNotification}
                />
              </View>
            )}
          />

          <List.Item
            title="Dark Theme"
            titleStyle={{color: theme.text, ...FONTS.h4}}
            description="Toggle theme"
            descriptionStyle={{color: theme.text, ...FONTS.body5}}
            right={props => (
              <View>
                <Switch
                  value={isThemeSwitchOn || theme.state}
                  onValueChange={onToggleTheme}
                />
              </View>
            )}
          />
        </View>
      </Container>
    </ThemeProvider>
  );
};

export default Settings;

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ScrollContainer = styled.ScrollView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const Text = styled.Text`
  font-size: 20px;
  font-weight: 500;
  color: ${props => props.theme.text};
`;

const SettingBox = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-vertical: 12px;
  margin-horizontal: 10px;
  border-bottom-width: 0.3px;
`;
