import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
  Alert,
  StatusBar,
  TextInput,
  Text,
  Dimensions,
  ScrollView,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {Divider, Button} from 'react-native-paper';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Fontisto';
import firebase from 'firebase/app';
import auth from '@react-native-firebase/auth';
import {v4 as uuidv4} from 'uuid';

import Fire from './../../components/firebase/firebaseConfig';
import {IMAGES, FONTS, PLACEHOLDERS} from './../../shared';
import {Permission, PERMISSION_TYPE} from './../../utils/UserPermissions';
import {selectTheme} from './../../redux/reducers/ThemeSlice';
const {width, height} = Dimensions.get('screen');

export default function UpdateProfile() {
  const [data, setData] = useState({
    username: '',
    uid: '',
    avatar: null,
    phoneNumber: '',
    check_usernameChange: false,
    check_phoneInputChange: false,
  });

  const theme = useSelector(selectTheme);
  const userData = auth().currentUser;
  const db = firebase.firestore().collection('USER').doc(userData.uid);

  // Enable user to pick an image after granting permission
  const handlePickAvatar = async () => {
    Permission.checkPermission(PERMISSION_TYPE.camera);
    let result = launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: 200,
        maxWidth: 200,
      },
      response => {
        setData({
          ...data,
          avatar: response.uri,
        });
        console.log('AVATAR: ', data.avatar);
      },
    );
  };

  const usernameChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        username: val,
        check_usernameChange: true,
      });
    } else {
      setData({
        ...data,
        username: val,
        check_usernameChange: false,
      });
    }
  };

  const phoneInputChange = val => {
    if (val.length !== 0) {
      setData({
        ...data,
        phoneNumber: val,
        check_phoneInputChange: true,
      });
    } else {
      setData({
        ...data,
        phoneNumber: val,
        check_phoneInputChange: false,
      });
    }
  };

  function updateProfile() {
    try {
      if (data.avatar !== null) {
        updateAvatar();
      }
      if (data.username !== '') {
        updateUsername();
      }
      if (data.phoneNumber !== '') {
        updatePhoneNumber();
      }
    } catch (err) {
      alert(err);
    }
  }

  // Update avatar
  const updateAvatar = async () => {
    let remoteUri = null;

    if (data.avatar) {
      const fileExtension = data.avatar.split('.').pop();

      // File name
      var uuid = uuidv4();
      const fileName = `${uuid}.${fileExtension}`;

      // Upload image to storage, then get referance
      remoteUri = await Fire.shared.uploadPhotoAsync(
        data.avatar,
        `Users/${userData.email}/images/avatars/${fileName}`,
      );

      db.set({avatar: remoteUri}, {merge: true});

      userData
        .updateProfile({photoURL: remoteUri})
        .then(function () {
          // Update successful.
          Alert.alert('Avatar Update Successful');
        })
        .catch(function (error) {
          // An error happened.
          alert(error);
        });
    }
    setData({
      ...data,
      avatar: null,
    });
  };

  // Update username
  function updateUsername() {
    db.set({name: data.username}, {merge: true});

    userData
      .updateProfile({
        displayName: data.username,
      })
      .then(function () {
        // Update successful.
        Alert.alert('Username Update Successful');
      })
      .catch(function (error) {
        // An error happened.
        alert(error);
      });
  }

  // Update phone number
  function updatePhoneNumber() {
    db.set({phoneNumber: data.phoneNumber}, {merge: true});

    userData
      .updatePhoneNumber({phoneNumber: data.phoneNumber})
      .then(function () {
        // Update successful.
        Alert.alert('Phone Number Update Successful');
      })
      .catch(function (error) {
        // An error happened.
        alert(error);
      });
  }

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />

        <Animatable.View
          animation="zoomIn"
          style={[styles.footer, {backgroundColor: theme.background}]}>
          {/* Form */}
          <ScrollView>
            {/* Avatar */}
            <View
              style={[
                styles.action,
                {alignItems: 'center', justifyContent: 'center'},
              ]}>
              <View
                style={[
                  styles.header,
                  {
                    backgroundColor: theme.background,
                  },
                ]}>
                <TouchableOpacity>
                  <ContactAvatar
                    style={{height: 100, width: 100, padding: 5}}
                    source={
                      data.avatar ? {uri: data.avatar} : IMAGES.tempAvatar
                    }
                  />
                  <View
                    style={[styles.viewIcon, {backgroundColor: theme.primary}]}>
                    <Icon
                      name="camera"
                      size={34}
                      style={styles.modifyAvatar}
                      onPress={handlePickAvatar}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            {/* Username */}
            <View style={styles.action}>
              <Icon name="person" color={theme.text} size={20} />
              <TextInput
                placeholder={PLACEHOLDERS.username}
                placeholderTextColor="gray"
                style={[styles.textInput, {color: theme.text}]}
                autoCapitalize="none"
                onChangeText={val => usernameChange(val)}
              />
              {data.check_usernameChange ? (
                <Animatable.View animation="bounceIn">
                  <Icon
                    name="check-circle"
                    color={theme.onBackground}
                    size={20}
                  />
                </Animatable.View>
              ) : null}
            </View>

            {/* Phone number */}
            <View style={styles.action}>
              <Icon name="phone" color={theme.text} size={20} />
              <TextInput
                placeholder={PLACEHOLDERS.phone}
                placeholderTextColor="gray"
                style={[styles.textInput, {color: theme.text}]}
                keyboardType="phone-pad"
                autoCapitalize="none"
                onChangeText={val => phoneInputChange(val)}
              />
              {data.check_phoneInputChange ? (
                <Animatable.View animation="bounceIn">
                  <Icon
                    name="check-circle"
                    color={theme.onBackground}
                    size={20}
                  />
                </Animatable.View>
              ) : null}
            </View>

            {/* Buttons */}
            <ButtonsBox>
              <Button
                style={{borderColor: theme.text}}
                color={theme.text}
                mode="outlined"
                onPress={() => {
                  updateProfile();
                }}>
                Update
              </Button>
            </ButtonsBox>
          </ScrollView>
        </Animatable.View>
      </Container>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  modify: {
    paddingTop: 2,
    paddingBottom: 2,
    fontSize: 14,
  },
  headerTitle: {
    fontSize: 20,
    fontWeight: '500',
  },
  header: {
    paddingTop: 10,
    paddingBottom: 16,
    shadowOffset: {height: 5},
    shadowRadius: 15,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  button: {
    marginTop: 20,
    marginHorizontal: 50,
    marginBottom: 50,
  },
  middle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textSize: {
    fontSize: 18,
  },
  viewIcon: {
    backfaceVisibility: 'visible',
    right: 5,
    bottom: 5,
    position: 'absolute',
    borderRadius: 50,
  },
  modifyAvatar: {
    padding: 10,
    fontSize: 18,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  footer: {
    flex: Platform.OS === 'ios' ? 3 : 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 0,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20,
  },
  color_textPrivate: {
    color: 'grey',
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ContactAvatar = styled.Image`
  border-radius: 100px;
`;

const Centered = styled.View`
  align-items: center;
  justify-content: center;
`;

const ButtonsBox = styled(Centered)`
  margin-top: 40px;
`;
