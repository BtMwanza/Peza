import React from 'react';
import {
  View,
  Dimensions,
  FlatList,
  StyleSheet,
  StatusBar,
  Animated,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Button, Text} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import Icon from 'react-native-vector-icons/Feather';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {useNavigation} from '@react-navigation/native';

import {
  deleteItem,
  increaseQuantity,
  decreaseQuantity,
  clearCart,
} from './../../redux';
import {SIZES, FONTS, BUTTONS, ROUTES} from '../../shared';
import {selectTheme} from './../../redux/reducers/ThemeSlice';
import {selectCart} from './../../redux/reducers/CartSlice';

const {width} = Dimensions.get('window').width;
const {height} = Dimensions.get('window').height;

function Cart() {
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const dispatch = useDispatch();
  const totalPrice = getTotal();
  const navigation = useNavigation();

  const quantityUp = item => {
    let idx = cart.indexOf(item);
    try {
      dispatch(increaseQuantity(idx));
    } catch (error) {
      alert(error);
    }
  };

  const quantityDown = item => {
    let idx = cart.indexOf(item);
    try {
      dispatch(decreaseQuantity(idx));
    } catch (error) {
      alert(error);
    }
  };

  const deleteCartItem = item => {
    try {
      dispatch(deleteItem(item));
    } catch (error) {
      alert(error);
    }
  };

  function getTotal() {
    return cart
      .reduce((acc, item) => {
        return acc + item.price;
      }, 0)
      .toFixed(2);
  }

  // Delete item from the cart
  const rightActions = (dragX, item, index) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0.9],
      extrapolate: 'clamp',
    });

    const opacity = dragX.interpolate({
      inputRange: [-100, -20, 0],
      outputRange: [1, 0.9, 0],
      extrapolate: 'clamp',
    });
    return (
      <TouchableOpacity onPress={() => deleteCartItem(index)}>
        <Animated.View
          style={[
            styles.deleteBtn,
            {opacity: opacity, backgroundColor: theme.background},
          ]}>
          <Icon
            name="trash-2"
            size={30}
            color="red"
            backgroundColor={theme.top_tab}
          />
        </Animated.View>
      </TouchableOpacity>
    );
  };

  const close = () => {};

  function renderCartDisplay(item, index) {
    return (
      <Swipeable
        containerStyle={{marginVertical: 1}}
        renderRightActions={(_, dragX) => rightActions(dragX, item, index)}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            width: '100%',
          }}>
          {/* Image */}
          <LeftContainer>
            <ProductImage source={{uri: item.image}} />
          </LeftContainer>

          {/* Right side components */}
          <View style={{flex: 1, width: '100%'}}>
            <RightContainer>
              {/* Left Side */}
              <LeftBox>
                <ProductName style={{...FONTS.body5}}>
                  {item.productName}
                </ProductName>
                <ProductPrice style={{...FONTS.body5}}>
                  K{item.price}
                </ProductPrice>
              </LeftBox>

              {/* Right Side */}
              <RightBox>
                <Icon
                  name="plus"
                  size={25}
                  style={{
                    height: 25,
                    width: 25,
                    borderRadius: 25,
                    backgroundColor: theme.primary,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  color={theme.onPrimary}
                  backgroundColor={theme.top_tab}
                  onPress={() => quantityUp(item)}
                />

                <Text
                  numberOfLines={1}
                  style={{
                    color: theme.text,
                    ...FONTS.body5,
                    overflow: 'hidden',
                  }}>
                  {item.currentQuantity}
                </Text>

                <Icon
                  name="minus"
                  style={{
                    height: 25,
                    width: 25,
                    borderRadius: 25,
                    backgroundColor: theme.primary,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  size={25}
                  color={theme.onPrimary}
                  backgroundColor={theme.top_tab}
                  onPress={() => quantityDown(item)}
                />
              </RightBox>
            </RightContainer>
          </View>
        </View>
      </Swipeable>
    );
  }
  return (
    <ThemeProvider theme={theme}>
      <Container>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.status_bar}
        />
        <FlatList
          showsVerticalScrollIndicator={false}
          data={cart}
          keyExtractor={item => ' ' + item.productID}
          renderItem={({item, index}) => renderCartDisplay(item, index)}
          horizontal={false}
        />
        <FooterBox>
          <Text style={{color: theme.text, fontWeight: '600', ...FONTS.h2}}>
            Total Price: K{totalPrice}
          </Text>

          <Button
            style={{borderColor: theme.accent}}
            mode="text"
            color={theme.text}
            onPress={() => navigation.navigate(ROUTES.checkout)}>
            {BUTTONS.proceed}
          </Button>
        </FooterBox>
      </Container>
    </ThemeProvider>
  );
}

export default Cart;

const styles = StyleSheet.create({
  deleteBtn: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 80,
  },
});

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const ProductImage = styled.Image`
  width: 60px;
  height: 60px;
  resize-mode: contain;
  border-color: ${props => props.theme.background};
`;

const ProductName = styled.Text`
  font-weight: 900;
  flex-wrap: wrap;
  font-size: ${SIZES.h4}px;
  margin-top: ${SIZES.radius}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const ProductPrice = styled.Text`
  font-size: ${SIZES.h5}px;
  margin-top: ${SIZES.radius}px;
  margin-horizontal: ${SIZES.padding}px;
  color: ${props => props.theme.text};
`;

const LeftBox = styled.View`
  margin-horizontal: 4px;
  justify-content: space-between;
`;

const RightBox = styled.View`
  flex-direction: column;
  margin-horizontal: 6px;
  align-items: center;
`;

const FooterBox = styled.View`
  flex-direction: row;
  border-top-left-radius: 30px;
  border-top-right-radius: 30px;
  border-bottom-left-radius: 30px;
  border-bottom-right-radius: 30px;
  padding-horizontal: 15px;
  padding-vertical: 0px;
  align-items: center;
  justify-content: space-between;
  bottom: 5px;
  height: 45px;
  background-color: ${props => props.theme.surface};
`;

const RightContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding-vertical: 4px;
  background-color: ${props => props.theme.surface};
  max-width: 100%;
`;

const LeftContainer = styled.View`
  background-color: ${props => props.theme.surface};
`;
