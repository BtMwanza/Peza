import React, {useEffect, useState, useLayoutEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Image,
  ActivityIndicator,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import styled, {ThemeProvider} from 'styled-components';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import SimplePicker from 'react-native-simple-picker';
import {Button} from 'react-native-paper';
import firebase from 'firebase/app';

import {SIZES, FONTS, ROUTES, IMAGES} from './../../shared';
import {fetchCurrentUser} from './../../redux/reducers/AuthSlice';
import {
  fetchRecentProducts,
  fetchVendor,
} from './../../redux/reducers/ProductSlice';
import {selectTheme} from './../../redux/reducers/ThemeSlice';
import {selectCart} from './../../redux/reducers/CartSlice';
import CacheImage from './../../components/CacheImage';
import {
  filterList,
  fetchData,
  addItem,
  deleteItem,
  setVendorID,
  setCurrentProduct,
} from './../../redux';
import {selectProducts} from './../../redux/reducers/ProductSlice';
import CartIcon from './../../components/header/CartIcon';
import MenuIcon from './../../components/header/MenuIcon';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const Home = () => {
  // Define states
  const theme = useSelector(selectTheme);
  const cart = useSelector(selectCart);
  const {products, selectedCategory, categories, isLoading} = useSelector(
    selectProducts,
  );
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [data, setData] = useState({
    loading: false,
    productData: [],
    search: '',
    isActive: false,
  });

  // Dummy Data
  const [promo, setPromo] = React.useState([
    {
      productID: 23,
      productName: 'BMW Engine',
      oldPrice: 9000,
      price: 5000,
      image: IMAGES.bmwEngine,
      currentQuantity: 1,
      category: 'Engine & Emissions',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 24,
      productName: 'Bosch Double Iriduim Spark Plug',
      oldPrice: 200,
      price: 150,
      image: IMAGES.boschDoubleIriduimSparkPlug,
      currentQuantity: 1,
      category: 'Engine & Emissions',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 25,
      productName: 'Bosch Spark Plug',
      oldPrice: 199.99,
      price: 89.99,
      image: IMAGES.boschSparkPlug,
      currentQuantity: 1,
      category: 'Engine & Emissions',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 26,
      productName: 'Headlight',
      oldPrice: 320,
      price: 220,
      image: IMAGES.carHeadlights,
      currentQuantity: 1,
      category: 'Lamps & Lighting',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 27,
      productName: 'Dellorto Caburator',
      oldPrice: 550,
      price: 390,
      image: IMAGES.carburator,
      currentQuantity: 1,
      category: 'Engine & Emissions',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
    {
      productID: 28,
      productName: '2003-2008 Toyota Corolla Headlights',
      oldPrice: 300,
      price: 210,
      image: IMAGES.toyotaCorollaHeadlight03_08,
      currentQuantity: 1,
      category: 'Lamps & Lighting',
      desc:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'",
      vendor: 'jphphLx4XeaaP561SvbYfkJA8Tk2',
    },
  ]);
  // Search for a product(s)
  const searchFilter = text => {
    if (text) {
      const newData = data.productData.filter(item => {
        const itemData =
          item.productName || item.make || item.model || item.brand
            ? item.productName.toUpperCase() ||
              item.make.toUpperCase() ||
              item.model.toUpperCase() ||
              item.brand.toUpperCase()
            : ''.toLowerCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setData({...data, productData: newData, search: text});
    } else {
      setData({...data, productData: products, search: text});
    }
  };

  // Filter products based on selected category
  function handleSelected(index) {
    dispatch(filterList(index));

    let current = categories.find(({idx}) => idx === index);
    console.log('CURRENT: ', current.category);

    if (index !== 0) {
      let filter = products.filter(
        ({category}) => category === current.category,
      );
      setData({...data, productData: filter});
    } else {
      setData({...data, productData: products});
    }
  }

  // Function to add product to the cart
  const addToCart = item => {
    try {
      dispatch(addItem(item));
    } catch (error) {
      alert(error);
    }
  };

  // Function to remove product to the cart
  const removeFromCart = item => {
    let id = item.productID;
    try {
      dispatch(deleteItem(id));
    } catch (error) {
      alert(error);
    }
  };

  const toggleCart = item => {
    try {
      const added = cart.find(p => p.productID === item.productID);
      if (!added) {
        addToCart(item);
        setData({...data, isActive: true});
      } else {
        removeFromCart(item);
        setData({...data, isActive: false});
      }
    } catch (error) {
      console.log(error);
    }
  };

  // Get Product from firebase
  function fetchProductsData(params) {
    try {
      firebase
        .firestore()
        .collection('PRODUCTS')
        .get()
        .then(querySnapshot => {
          const productList = [];
          querySnapshot.forEach(doc => {
            productList.push({
              key: doc.id,
              productID: doc.id,
              currentQuantity: parseInt(doc.data().currentQuantity),
              vendorID: doc.data().vendor,
              productName: doc.data().productName,
              image: doc.data().image,
              price: doc.data().price,
              desc: doc.data().desc,
              category: doc.data().category,
              createdAt: doc.data().createdAt,
              isSold: doc.data().isSold,
              productCode: doc.data().productCode,
              brand: doc.data().brand,
              vin: doc.data().VIN,
              year: doc.data().year,
              make: doc.data().make,
              model: doc.data().model,
              extraInfo: doc.data().extraInfo,
            });
          });

          setData({...data, productData: productList});
          dispatch(fetchData(productList));
        });
    } catch (error) {
      alert(error.message);
    }
  }

  // Function to render the display of the products in the flatlist
  const List = React.memo(
    ({key, image, productName, vendorID, item, price}) => {
      return (
        <ProductBox
          style={{
            ...styles.shadow,
          }}>
          <LikeBox>
            <Icon
              name={'heart'}
              color={theme.text}
              style={{
                backgroundColor: theme.surface,
                position: 'absolute',
                top: 0,
                right: 0,
              }}
              size={20}
            />
          </LikeBox>
          <TouchableOpacity
            onPress={() => {
              dispatch(setVendorID(vendorID)),
                dispatch(setCurrentProduct(item)),
                dispatch(fetchVendor(vendorID)),
                console.log('PRICE: ', price);
              navigation.navigate(ROUTES.productDetails, {
                item,
              });
            }}>
            <CacheImage style={styles.image} uri={image} />
          </TouchableOpacity>

          <DetialsView>
            <ProductName
              numberOfLines={2}
              style={{
                ...FONTS.h3,
              }}>
              {productName}
            </ProductName>
          </DetialsView>
          <ProductPrice style={{...FONTS.body4}}>K{price}</ProductPrice>
          {/* ADD TO CART BUTTON */}
          <CartBox onPress={() => toggleCart(item)}>
            <Icon
              name={'shopping-bag'}
              color={data.isActive === false ? theme.text : theme.primary}
              style={{backgroundColor: 'transparent'}}
              size={20}
            />
          </CartBox>
        </ProductBox>
      );
    },
  );

  useEffect(() => {
    dispatch(fetchCurrentUser());
    dispatch(fetchRecentProducts());
    fetchProductsData();
    return () => {};
  }, []);

  // TEST UI
  // Main view
  return (
    <ThemeProvider theme={theme}>
      <StatusBar
        backgroundColor={theme.background}
        barStyle={theme.status_bar}
      />
      <Container>
        <Navbar>
          <View>
            <MenuIcon
              navigation={navigation}
              onPress={() => navigation.openDrawer()}
            />
          </View>

          <View>
            <SearchInput
              style={{color: theme.text, ...FONTS.body4}}
              value={data.search}
              placeholder="Search..."
              placeholderTextColor={theme.text}
              onChangeText={text => searchFilter(text)}
            />
          </View>

          <View>
            <CartIcon navigation={navigation} />
          </View>
        </Navbar>
        {/* CATEGORIES */}
        <View style={{marginTop: 0, top: 0}}>
          <CategoryBox
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {categories.map((item, index) => {
              return (
                <TouchableOpacity
                  style={{
                    marginBottom: 5,
                    padding: 2,
                  }}
                  onPress={() => handleSelected(index)}>
                  <Text
                    style={{
                      marginRight: 15,
                      opacity: index === selectedCategory ? 1 : 0.5,
                      color: theme.text,
                      ...FONTS.h3,
                    }}>
                    {item.category}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </CategoryBox>
        </View>

        <FlatList
          columnWrapperStyle={{
            justifyContent: 'space-between',
            paddingHorizontal: 20,
          }}
          contentContainerStyle={{paddingTop: 10}}
          key={2}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          data={data.productData}
          keyExtractor={(item, index) => item.productID.toString()}
          renderItem={({item, index}) => {
            return (
              <List
                item={item}
                productName={item.productName}
                image={item.image}
                price={item.price}
                vendorID={item.vendorID}
              />
            );
          }}
          horizontal={false}
        />
      </Container>
    </ThemeProvider>
  );
};

export default Home;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3.84,
    elevation: 8,
  },
  image: {
    height: 100,
    aspectRatio: 1,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});

// STYLED COMPONENTS
const Navbar = styled.View`
  top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 4px;
  margin-bottom: 10px;
`;
const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.background};
`;

const CategoryBox = styled.ScrollView`
  margin-vertical: 5px;
  margin-horizontal: 7px;
`;

const ProductList = styled.View`
  margin-vertical: 10px;
  margin-bottom: 100px;
`;

const ProductBox = styled.View`
  background-color: ${props => props.theme.surface};
  width: ${SIZES.cardWidth}px;
  margin-bottom: 20px;
  height: ${SIZES.cardHeight}px;
  padding: 10px;
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
`;

const ProductImage = styled.Image`
  height: 100px;
  aspect-ratio: 1;
  resize-mode: contain;
  align-self: center;
`;

const DetialsView = styled.View`
  margin-top: 10px;
  justify-content: space-between;
`;

const ProductName = styled.Text`
  color: ${props => props.theme.onSurface};
  flex-wrap: wrap;
`;

const ProductDesc = styled.Text`
  font-size: ${EStyleSheet.value('1rem')}px;
  opacity: 0.7;
  z-index: 1;
  color: ${props => props.theme.onSurface};
`;

const ProductPrice = styled.Text`
  font-size: 16px;
  color: ${props => props.theme.onSurface};
  position: absolute;
  bottom: 10px;
  left: 10px;
`;

const CartBox = styled.TouchableOpacity`
  aspect-ratio: 1;
  background-color: ${props => props.theme.surface};
  position: absolute;
  bottom: 10px;
  right: 10px;
  justify-content: center;
  align-items: center;
`;

const LikeBox = styled.TouchableOpacity`
  background-color: ${props => props.theme.surface};
  height: 24px;
  top: -5px;
  left: 0px;
`;

const SearchBox = styled.View`
  margin-vertical: 1px;
  margin-horizontal: 4px;
  width: 300px;
`;

const SearchInput = styled.TextInput`
  height: 40px;
  width: ${WIDTH / 1.4}px;
  border-width: 1px;
  padding: 5px;
  padding-horizontal: 20px;
  border-color: #009688;
  border-top-right-radius: 25px;
  border-top-left-radius: 25px;
  border-bottom-left-radius: 25px;
  border-bottom-right-radius: 25px;
`;

const SearchButton = styled.View`
  position: absolute;
  top: 10px;
  right: 20px;
`;
