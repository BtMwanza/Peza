import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Login, Register, Loading} from '../screens';

export default function AuthStack({navigation}) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator headerMode="none" initialRouteName="Login">
      <Stack.Screen name="Loading" component={Loading} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  );
}
