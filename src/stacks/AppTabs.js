import React from 'react';
import {getFocusedRouteNameFromRoute} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Feather';
import {useSelector, useDispatch} from 'react-redux';
import {View} from 'react-native';

import {
  Home,
  ProductDetails,
  Profile,
  Settings,
  Explore,
  Cart,
  Checkout,
  Map,
  Loading,
  UpdateProfile,
  VendorInfo,
  Transactions,
  TransactionDetails,
  Payment,
  Summary,
} from '../screens';
import {CartIcon, SearchIcon} from './../components';
import {ROUTES, FONTS} from './../shared';

const Stack = createStackNavigator();

function getHeaderTitle(route) {
  // In case the focused route is not found, assume it's the first screen
  return getFocusedRouteNameFromRoute(route);
}

function AppTabs() {
  const theme = useSelector(state => state.themer.theme);
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerStyle: {
          backgroundColor: theme.top_tab,
        },
        headerTintColor: theme.text,
        headerTitleStyle: {
          fontWeight: '600',
          ...FONTS.navTitle,
        },
      }}
      mode="modal">
      <Stack.Screen
        name="Home"
        component={Home}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.openDrawer()}
            />
          ),
          headerShown: false,
          // Pass tab name when tab is pressed, here!
          /*  title: getHeaderTitle(route),
          headerRight: props => (
            <View style={{flexDirection: 'row'}}>
              <CartIcon navigation={navigation} />
            </View>
          ), */
        })}
      />
      <Stack.Screen
        name="ProductDetails"
        component={ProductDetails}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="arrow-left"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.goBack()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
          headerRight: props => (
            <View style={{flexDirection: 'row'}}>
              <CartIcon navigation={navigation} />
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="Explore"
        component={Explore}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.openDrawer()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
          headerRight: () => (
            <View style={{flexDirection: 'row'}}>
              <CartIcon navigation={navigation} />
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.openDrawer()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
        })}
      />
      <Stack.Screen
        name="UpdateProfile"
        component={UpdateProfile}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="arrow-left"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.goBack()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
        })}
      />
      <Stack.Screen
        name="Settings"
        component={Settings}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.openDrawer()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
          headerRight: () => (
            <View style={{flexDirection: 'row'}}>
              <CartIcon navigation={navigation} />
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="Cart"
        component={Cart}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.openDrawer()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
          headerRight: () => (
            <View style={{flexDirection: 'row'}}>
              <CartIcon navigation={navigation} />
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="Checkout"
        component={Checkout}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="arrow-left"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.goBack()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
          headerRight: () => (
            <View style={{flexDirection: 'row'}}>
              <CartIcon navigation={navigation} />
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="Map"
        component={Map}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.openDrawer()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
          headerRight: () => (
            <View style={{flexDirection: 'row'}}>
              <CartIcon navigation={navigation} />
            </View>
          ),
        })}
      />
      <Stack.Screen
        name="VendorInfo"
        component={VendorInfo}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="arrow-left"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.goBack()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
        })}
      />
      <Stack.Screen
        name="Payment"
        component={Payment}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="arrow-left"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.goBack()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
        })}
      />
      <Stack.Screen
        name="Summary"
        component={Summary}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="arrow-left"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.goBack()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
        })}
      />
      <Stack.Screen
        name="Transactions"
        component={Transactions}
        options={({navigation, route}) => ({
          headerLeft: () => (
            <Icon.Button
              name="arrow-left"
              size={25}
              color={theme.text}
              backgroundColor={theme.top_tab}
              onPress={() => navigation.goBack()}
            />
          ),
          // Pass tab name when tab is pressed, here!
          title: getHeaderTitle(route),
        })}
      />
      <Stack.Screen name="TransactionDetails" component={TransactionDetails} />
      <Stack.Screen name="Loading" component={Loading} />
    </Stack.Navigator>
  );
}

export default AppTabs;
