import {createSlice} from '@reduxjs/toolkit';
import {lightTheme, darkTheme} from '../../components/Themes';

export const themeSlice = createSlice({
  name: 'themer',
  initialState: {
    theme: lightTheme,
  },
  reducers: {
    switchTheme: (state, action) => {
      state.theme = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {switchTheme} = themeSlice.actions;

export const selectTheme = state => state.themer.theme;

export default themeSlice.reducer;
