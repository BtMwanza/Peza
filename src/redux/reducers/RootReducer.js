import {combineReducers} from 'redux';

import authReducer from './AuthSlice';
import cartReducer from './CartSlice';
import productReducer from './ProductSlice';
import themeReducer from './ThemeSlice';

const rootReducer = combineReducers({
  themer: themeReducer,
  auth: authReducer,
  cart: cartReducer,
  prodSlice: productReducer,
});

export default rootReducer;
