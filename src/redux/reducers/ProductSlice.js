import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import firebase from 'firebase/app';

// Inntial states
const initialState = {
  products: [],
  recentProducts: [],
  vendorData: [],
  vendors: [],
  filteredList: [],
  categories: [
    {idx: 0, category: 'All'},
    {idx: 1, category: 'Engine & Emissions'},
    {idx: 2, category: 'Lighting'},
    {idx: 3, category: 'Brakes & Suspension'},
    {idx: 4, category: 'Tyres & Rims'},
    {idx: 5, category: 'Wiring'},
    {idx: 6, category: 'Electrical'},
  ],
  viewed: [],
  popular: {},
  currentProduct: {},
  isLoading: false,
  selectedCategory: 0,
  vendorID: '',
  searchText: '',
};

// Get Product from firebase
export const fetchRecentProducts = createAsyncThunk(
  'prodSlice/fetchRecentProducts',
  async () => {
    const getProducts = firebase
      .firestore()
      .collection('PRODUCTS')
      .orderBy('createdAt', 'asc')
      .limit(7)
      .get()
      .then(querySnapshot => {
        const productList = [];
        querySnapshot.forEach(doc => {
          productList.push({
            key: doc.id,
            productID: doc.id,
            currentQuantity: parseInt(doc.data().currentQuantity),
            vendorID: doc.data().vendor,
            productName: doc.data().productName,
            image: doc.data().image,
            price: parseFloat(doc.data().price),
            desc: doc.data().desc,
            category: doc.data().category,
            createdAt: doc.data().createdAt,
            isSold: doc.data().isSold,
            productCode: doc.data().productCode,
            brand: doc.data().brand,
            vin: doc.data().VIN,
            year: doc.data().year,
            make: doc.data().make,
            model: doc.data().model,
            extraInfo: doc.data().extraInfo,
          });
        });
        return productList;
      });

    const data = await getProducts;
    return data;
  },
);

// Get vendor data
export const fetchVendor = createAsyncThunk(
  'prodSlice/fetchVendor',
  async vendorID => {
    const getVendor = firebase
      .firestore()
      .collection('VENDORS')
      .get()
      .then(querySnapshot => {
        const data = [];
        console.log('FETCHED_ID', vendorID);
        querySnapshot.forEach(doc => {
          if (doc.id === vendorID) {
            data.push({
              key: doc.id,
              uid: doc.id,
              displayName: doc.data().name,
              address: doc.data().address,
              aboutUs: doc.data().aboutUs,
              email: doc.data().email,
              avatar: doc.data().avatar,
              phoneNumber: doc.data().phoneNumber,
              location: doc.data().location,
            });
          }
        });
        return data;
      });
    const vendor = await getVendor;
    return vendor;
  },
);

// Get vendors
export const fetchVendors = createAsyncThunk(
  'prodSlice/fetchVendors',
  async vendorID => {
    const getVendor = firebase
      .firestore()
      .collection('VENDORS')
      .get()
      .then(querySnapshot => {
        const data = [];
        querySnapshot.forEach(doc => {
          data.push({
            key: doc.id,
            uid: doc.id,
            displayName: doc.data().name,
            address: doc.data().address,
            aboutUs: doc.data().aboutUs,
            email: doc.data().email,
            avatar: doc.data().avatar,
            phoneNumber: doc.data().phoneNumber,
            location: doc.data().location,
          });
        });
        return data;
      });
    const vendors = await getVendor;
    return vendors;
  },
);

export const productSlice = createSlice({
  name: 'prodSlice',
  initialState,
  reducers: {
    fetchData: (state, action) => {
      state.isLoading = true;
      state.products = action.payload;
    },
    filterList: (state, action) => {
      state.selectedCategory = action.payload;
    },
    searchList: (state, action) => {
      state.searchText = action.payload;
    },
    setVendorID: (state, action) => {
      state.vendorID = action.payload;
    },
    setCurrentProduct: (state, action) => {
      state.currentProduct = action.payload;
      const seen = state.viewed.find(
        ({productID}) => productID === state.currentProduct.productID,
      );

      let view = {
        productID: state.currentProduct.productID,
        count: 0,
      };
      if (!seen) {
        view.count = 1;
        state.viewed.push(view);
      } else {
        seen.count++;
      }
    },
  },
  extraReducers: {
    [fetchRecentProducts.fulfilled]: (state, action) => {
      state.recentProducts = action.payload;
    },
    [fetchVendor.fulfilled]: (state, action) => {
      state.vendorData = action.payload;
    },
    [fetchVendors.fulfilled]: (state, action) => {
      state.vendors = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  fetchData,
  filterList,
  searchList,
  setVendorID,
  setCurrentProduct,
} = productSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectProducts = state => state.prodSlice;

export default productSlice.reducer;
