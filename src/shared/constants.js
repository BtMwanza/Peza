import { Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export const TITLES = {
  login: 'Sign in',
  register: 'Sign up',
  confirm: 'Confirm user',
  forget: 'Forget password',
  newPassword: 'New password',
};

export const MESSAGES = {
  forget: 'Forget password ?',
  register: 'Create an account',
  already: 'Already a member ?',
};

export const COLORS = {
  bright: '#fafafa',
  dark: '#33395F',
  blue: '#1760E3',
  creamy: '#e6ebf0',
  grey: '#b5b1b3',
};

export const BUTTONS = {
  login: 'Sign in',
  logout: 'Sing out',
  code: 'Send code',
  confirm: 'Confirm',
  send: 'Send code',
  register: 'Register',
  resend: 'Resend code',
  cart: 'Add to cart',
  pay: 'Pay',
  proceed: 'Proceed',
};

export const PLACEHOLDERS = {
  username: 'Username',
  fullname: 'Full Name',
  password: 'Password',
  confirmPassword: 'Password',
  code: 'Confirmation code',
  oldPassword: 'Old password',
  newPassword: 'New password',
  email: 'Email',
  phone: '260999000000',
  address: 'Address',
};

export const ROUTES = {
  auth: 'Auth',
  app: 'App',
  home: 'Home',
  loader: 'Loading',
  profile: 'Profile',
  register: 'Register',
  login: 'Login',
  explore: 'Explore',
  settings: 'Settings',
  feed: 'Feed',
  cart: 'Cart',
  checkout: 'Checkout',
  map: 'Map',
  productDetails: 'ProductDetails',
  editProfile: 'UpdateProfile',
  vendor: 'VendorInfo',
  transactions: 'Transactions',
  transactiondetails: 'TransactionDetails',
  summary: 'Summary',
  payment: 'Payment',
};

export const IMAGES = {
  bmwEngine: require('./../../assets/images/bmw-engine.png'),
  boschDoubleIriduimSparkPlug: require('./../../assets/images/bosch-double-iridium-spark-plug.png'),
  engineValveSprings: require('./../../assets/images/engine-valve-springs.png'),
  exhaustValveKit: require('./../../assets/images/supertech-inconel-exhaust-valve-kit.png'),
  boschSparkPlug: require('./../../assets/images/bosch-spark-plug.png'),
  headlights: require('./../../assets/images/headlights.png'),
  notAvailable: require('./../../assets/images/not-available.jpg'),
  carburator: require('./../../assets/images/Carburetor-Dellorto.png'),
  toyotaCorollaHeadlight03_08: require('./../../assets/images/03-08-toyota-corolla-euro-style-crystal-headlights.png'),
  falconWilwoodDiscBrake: require('./../../assets/images/xr-xt-xw-xy-falcon-wilwood-355mm-6-piston-disc-brake.png'),
  fordInjector: require('./../../assets/images/2003-2007-ford-industrial-injector.png'),
  tempAvatar: require('./../../assets/images/tempAvatar.jpg'),

  // ICONS
  engineIcon: require('./../../assets/images/car-engine.png'),
  brakesIcon: require('./../../assets/images/disc-brake.png'),
  headlightIcon: require('./../../assets/images/headlights-icon.png'),
  wireIcon: require('./../../assets/images/wire.png'),
  tyreIcon: require('./../../assets/images/tyre.png'),
  batteryIcon: require('./../../assets/images/accumulator.png'),
};

export const SIZES = {
  // global SIZES
  base: 4,
  radius: 8,
  padding: 12,
  font: 14,
  cardWidth: WIDTH / 2.4,
  // cardHeight: 215,
  cardHeight: 219,

  // font SIZES
  navTitle: 24,
  h1: 24,
  h2: 22,
  h3: 18,
  h4: 16,
  h5: 14,
  h6: 12,
  body1: 24,
  body2: 20,
  body3: 18,
  body4: 16,
  body5: 14,
  body6: 12,
  body7: 10,
  body8: 8,

  // app dimensions
  WIDTH,
  HEIGHT,
};

export const FONTS = {
  navTitle: {fontFamily: 'KoHo-Bold', fontSize: SIZES.navTitle},
  largeTitleBold: {fontFamily: 'KoHo-SemiBold', fontSize: SIZES.h2},
  h1: {fontFamily: 'KoHo-SemiBold', fontSize: SIZES.h1, lineHeight: 36},
  h2: {fontFamily: 'KoHo-SemiBold', fontSize: SIZES.h2, lineHeight: 30},
  h3: {fontFamily: 'KoHo-SemiBold', fontSize: SIZES.h3, lineHeight: 22},
  h4: {fontFamily: 'KoHo-SemiBold', fontSize: SIZES.h4, lineHeight: 22},
  h5: {fontFamily: 'KoHo-SemiBold', fontSize: SIZES.h5, lineHeight: 22},
  body1: {
    fontFamily: 'KoHo-Regular',
    fontSize: SIZES.body1,
    lineHeight: 36,
  },
  body2: {
    fontFamily: 'KoHo-Regular',
    fontSize: SIZES.body2,
    lineHeight: 30,
  },
  body3: {
    fontFamily: 'KoHo-Regular',
    fontSize: SIZES.body3,
    lineHeight: 22,
  },
  body4: {
    fontFamily: 'KoHo-Regular',
    fontSize: SIZES.body4,
    lineHeight: 22,
  },
  body5: {
    fontFamily: 'KoHo-Regular',
    fontSize: SIZES.body5,
    lineHeight: 22,
  },
  body6: {
    fontFamily: 'KoHo-Regular',
    fontSize: SIZES.body6,
    lineHeight: 22,
  },
  body7: {
    fontFamily: 'KoHo-Regular',
    fontSize: SIZES.body7,
  },
  body8: {
    fontFamily: 'KoHo-Regular',
    fontSize: SIZES.body8,
  },
};
