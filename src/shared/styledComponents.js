import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import {colors} from './constants';

const {width} = Dimensions.get('screen');

export const TextInput = styled.TextInput.attrs(props => ({
  autoCorrect: false,
  autoCapitalize: 'none',
  returnKeyType: 'done',
  placeholder: props.placeholder,
  secureTextEntry: props.password,
}))`
  font-size: 18px;
  border-color: ${colors.bright};
  border-width: 0.5px;
  border-radius: 30px;
  background-color: ${colors.bright};
  height: 55px;
  width: ${width * 0.9}px;
  padding: 16px;
  padding-left: 24px;
  margin-bottom: 16px;
  margin-top: 16px;
`;
