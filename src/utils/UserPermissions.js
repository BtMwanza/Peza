import {check, PERMISSIONS, request, RESULTS} from 'react-native-permissions';
import {Platform} from 'react-native';

const PLATFORM_CAMERA_PERMISSIONS = {
  ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
  android: PERMISSIONS.ANDROID.CAMERA,
};

const REQUEST_PERMISSION_TYPE = {
  camera: PLATFORM_CAMERA_PERMISSIONS,
};

const PERMISSION_TYPE = {
  camera: 'camera',
};

class UserPermissions {
  checkPermission = async (type): Promise<boolean> => {
    console.log('checkPermission type: ', type);
    const permissions = REQUEST_PERMISSION_TYPE[type][Platform.OS];
    console.log('checkPermission permissions: ', permissions);
    if (!permissions) {
      return true;
    }
    try {
      const result = await check(permissions);
      console.log('checkPermission result: ', result);
      if (result === RESULTS.GRANTED) return true;
      return this.requestCameraPermission(permissions); // Request camera permission
    } catch (error) {
      console.log('checkPermission error: ', error);
      return false;
    }
  };

  requestCameraPermission = async (permissions): Promise<boolean> => {
    console.log('requestCameraPermission permission: ', permissions);
    try {
      const result = await request(permissions);
      console.log('requestCameraPermission result: ', result);
      return result === RESULTS.GRANTED;
    } catch (error) {
      console.log('requestCameraPermission error: ', error);
      return false;
    }
  };
}

const Permission = new UserPermissions();
export {Permission, PERMISSION_TYPE};
