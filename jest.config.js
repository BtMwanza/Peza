// jest.config.js
const {defaults} = require('jest-config');
module.exports = {
  // ...
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
  moduleNameMapper: {
    '^react-native$': 'react-native-web',
    '\\.svg': 'Peza/__tests__/svgMock.js',
  },
  // ...
  setupFiles: ['Peza/__tests__/setup.js'],
};
